-- drop database [erdb_er30_ss] 
/*
CREATE DATABASE [erdb_er30_ss] 
ON ( NAME = N'hoff_datData', FILENAME = N'F:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\erdb_er30.ss' ) AS SNAPSHOT OF [erdb_er30]
GO
*/
use master
go
alter database erdb_er30 set single_user with rollback immediate
restore database erdb_er30 from database_snapshot = 'erdb_er30_ss'
go





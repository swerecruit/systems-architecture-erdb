-- This script runs prior to the clean
-- Not all objects are handled properly - fixes can go here
SET NOCOUNT ON
GO

IF OBJECT_ID('dbo.StringConcat') IS NOT NULL
	DROP AGGREGATE dbo.StringConcat;
GO

DECLARE @cmd VARCHAR(500);
DISABLE TRIGGER ALL ON DATABASE;
WHILE 1=1
BEGIN
	SET @cmd = (SELECT TOP (1) 'DISABLE TRIGGER '+name+' ON '+OBJECT_SCHEMA_NAME(parent_id)+'.'+OBJECT_NAME(parent_id)
				FROM sys.triggers
				WHERE is_ms_shipped = 0
				AND parent_class = 1
				AND is_disabled = 0);
	IF @cmd IS NOT NULL
		EXEC(@cmd);
	ELSE
		BREAK;
END;
GO

﻿IF EXISTS(SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID('[dbo].[spGetDecryptedDirectDeposit]'))
EXECUTE('DROP PROCEDURE [dbo].[spGetDecryptedDirectDeposit]')

PRINT 'Creating/Updating PROCEDURE [dbo].[spGetDecryptedDirectDeposit]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Stephen Caldwell
-- Create date: 11/11/2013
-- Description:	Decrypts direct deposit information
-- =============================================
CREATE PROCEDURE [dbo].[spGetDecryptedDirectDeposit]
	-- Add the parameters for the stored procedure here
	@CandidateID int,
	@DirectDepositField1 varbinary(128),
	@DirectDepositField2 varbinary(128),
	@DirectDepositField3 varbinary(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	OPEN SYMMETRIC KEY DirectDeposit_Key DECRYPTION BY CERTIFICATE CandidateDirectDeposit
    -- Insert statements for procedure here
	SELECT convert(varchar, DECRYPTBYKEY(@DirectDepositField1, 1, CONVERT(varbinary, @CandidateID))) as DirectDepositField1,
			convert(varchar, DECRYPTBYKEY(@DirectDepositField2, 1, CONVERT(varbinary, @CandidateID))) as DirectDepositField2,
			convert(varchar, DECRYPTBYKEY(@DirectDepositField3, 1, CONVERT(varbinary, @CandidateID))) as DirectDepositField3
END

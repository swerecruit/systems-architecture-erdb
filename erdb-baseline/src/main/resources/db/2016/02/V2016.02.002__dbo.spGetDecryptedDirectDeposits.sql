﻿IF EXISTS(SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID('[dbo].[spGetDecryptedDirectDeposits]'))
EXECUTE('DROP PROCEDURE [dbo].[spGetDecryptedDirectDeposits]')

PRINT 'Creating/Updating PROCEDURE [dbo].[spGetDecryptedDirectDeposits]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Stephen Caldwell / Tim Desplenter
-- Create date: 2016.01.11
-- Description:	Returns decrypted direct deposit information for a Candidate
-- =============================================
CREATE PROCEDURE [dbo].[spGetDecryptedDirectDeposits]
	-- Add the parameters for the stored procedure here
	@CandidateID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	OPEN SYMMETRIC KEY DirectDeposit_Key DECRYPTION BY CERTIFICATE CandidateDirectDeposit

	SELECT	cdd.RecordID as ID,
			cdd.CandidateID,
			cdd.IsActive,
			cdd.PercentAmount,
			cdd.Amount,
			cdd.ExternalTransactionCode,
			cdd.AccountTypeID,
			ddat.Name as AccountTypeName,
			ddat.GPCode as AccountTypeGPCode,
			convert(varchar, DECRYPTBYKEY(cdd.DirectDepositField1, 1, CONVERT(varbinary, @CandidateID))) as DirectDepositField1,
			convert(varchar, DECRYPTBYKEY(cdd.DirectDepositField2, 1, CONVERT(varbinary, @CandidateID))) as DirectDepositField2,
			convert(varchar, DECRYPTBYKEY(cdd.DirectDepositField3, 1, CONVERT(varbinary, @CandidateID))) as DirectDepositField3
	FROM	dbo.CandidateDirectDeposit cdd
	INNER JOIN dbo.DirectDepositAccountType ddat on cdd.AccountTypeID = ddat.TypeID
	WHERE	CandidateID = @CandidateID
END

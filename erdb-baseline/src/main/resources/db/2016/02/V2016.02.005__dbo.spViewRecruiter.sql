﻿IF EXISTS(SELECT * FROM	dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[spViewRecruiter]'))
EXECUTE('DROP PROCEDURE [dbo].[spViewRecruiter]')

PRINT 'Creating/Updating PROCEDURE [dbo].[spViewRecruiter]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spViewRecruiter]
/********************************************************************************************************
Stored Procedure Name: spViewRecruiter
Stored Procedure Description:
	Displays basic candidate information by company and user id
Stored Procedure Create Date: 3/9/2011
Stored Procedure Author: JEP

Sample Exec Statement:
EXEC dbo.spViewRecruiter 
	@ReferenceID = 6
	,@UserID = '212AA64F-D94C-4125-B357-CAE353829A1A'

Stored Procedure Modification History:
By		Date		Description
JEP		4/7/2011	-Created
TD		5/23/2011	-Added EndDate and IncludedInPool
TD		7/29/2011	-Added BroadBean info
TD		11/4/2011	-Restrict join with Vendors table to enabled vendors
BMOORE	07/10/2012	-Removed redundant Email column
					-Added Recruiter Department columns
RJ		2/15/2013   -Added @AboutTypeName param
TD		3/21/2012	-Added DefaultAddress stuff
RJ		7/10/2013 -Fixed NotesCount
*********************************************************************************************************/
    (
      @ReferenceID INT ,
      @UserID UNIQUEIDENTIFIER,
      @AboutTypeName VARCHAR(50) = 'Recruiter'
    )
AS 
    BEGIN

        SET NOCOUNT ON;

        DECLARE @AboutTypeID SMALLINT

        SELECT  @AboutTypeID = AboutTypeID
        FROM    dbo.AboutTypes (NOLOCK)
        WHERE   AboutTypeName = @AboutTypeName

        SELECT  HR.HR_REP_ID RecruiterID ,
                HR.FIRST_NAME FirstName ,
                HR.MIDDLE_INITIAL MiddleInitial ,
                HR.LAST_NAME LastName ,
                COALESCE(HR.FIRST_NAME, '') + ' ' + COALESCE(HR.LAST_NAME, '') ShortName ,
                HR.EMAIL Email ,
                HR.TITLE Title ,
                HR.RecruiterTypeID ,
                HR.EntityID ,
                HR.PASSWORD Password ,
                HR.HOME_PHONE HomePhone ,
                HR.BUSINESS_PHONE BusinessPhone ,
                HR.MOBILE_PHONE MobilePhone ,
                HR.FAX Fax ,
                HR.AD_SOURCE Adsource ,
				HR.AdsourceAdditionalInfo ,
                HR.PhoneSystemUserName ,
                HR.PhoneSystemPassword ,
                HR.START_DATE StartDate ,
                V.VendorID ,
                HR.END_DATE EndDate ,
                HR.INCLUDED_IN_POOL IncludedInPool ,
                HR.Timestamp ,
                HR.BroadBeanUsername ,
                HR.BroadBeanPassword ,
                HR.TimeZone ,
				HR.ExternalPayrollID,
                LastNote.LastNoteID ,
                LastNote.LastNoteBody ,
                LastNote.LastNoteCreatedBy ,
                LastNote.LastNoteCreatedOn ,
                LastNote.LastNoteCreatedByName ,
                LastNoteByUser.LastNoteIDByUser ,
                LastNoteByUser.LastNoteBodyByUser ,
                LastNoteByUser.LastNoteCreatedOnByUser ,

                HR.DefaultAddressID AddressID ,
                DefaultAddress.CommunicationTypeID CommunicationTypeID,
                DefaultAddress.Name AddressName ,
                DefaultAddress.Street1 AddressLine1 ,
                DefaultAddress.Street2 AddressLine2 ,
                DefaultAddress.City City ,
                DefaultAddress.PostalCode PostalCode ,
                DefaultAddress.StateID StateID ,
                DefaultAddress.RegionID RegionID ,
                DefaultAddress.CountryID CountryID ,
                DefaultAddressState.Name StateName ,
                DefaultAddressRegion.Name RegionName ,
                DefaultAddressCountry.Name CountryName ,
                
                ( SELECT    COUNT(*)
                  FROM      dbo.AddressReferences AR ( NOLOCK )
                  WHERE     AR.AboutTypeID = @AboutTypeID
                            AND AR.ReferenceID = HR.HR_REP_ID
                ) AddressCount ,
                ( SELECT    COUNT(*)
                  FROM      dbo.NoteReferences RI ( NOLOCK )
                            JOIN dbo.NewNotes NI ( NOLOCK ) ON RI.NoteID = NI.NoteID
                            JOIN dbo.NoteActions NA ( NOLOCK ) ON NA.ActionID = NI.ActionID
                  WHERE     RI.AboutTypeID = @AboutTypeID
                            AND RI.ReferenceID = @ReferenceID
                            AND NA.Permission IS NULL
                ) NoteCount ,
                ( SELECT    COUNT(DISTINCT LI.ListID)
                  FROM      dbo.Lists LI ( NOLOCK )
                            JOIN dbo.ListMembers LMI ( NOLOCK ) ON LI.ListID = LMI.ListID
                            LEFT JOIN dbo.ListAccess LAI ( NOLOCK ) ON LI.ListID = LAI.ListID
                  WHERE     LI.AboutTypeID = @AboutTypeID
                            AND LMI.ReferenceID = HR.HR_REP_ID
                            AND ( CreatedBy = @UserID
                                  OR IsPublicRead = 1
                                  OR LAI.UserID = @UserID
                                )
                ) ListCount ,
                ( SELECT    COUNT(*)
                  FROM      dbo.ObjectRelationships RI ( NOLOCK )
                  WHERE     ( RI.AboutTypeIDFrom = @AboutTypeID
                              AND RI.ReferenceIDFrom = HR.HR_REP_ID
                            )
                            OR ( RI.AboutTypeIDTo = @AboutTypeID
                                 AND RI.ReferenceIDTo = HR.HR_REP_ID
                               )
                ) RelationshipCount ,
                ( SELECT    COUNT(*)
                  FROM      dbo.Attachments AI ( NOLOCK )
                            JOIN dbo.AttachmentTypes ATI ( NOLOCK ) ON AI.TypeID = ATI.TypeID
                  WHERE     AI.DeletedOn IS NULL
                            AND ParentAttachmentID IS NULL
                            AND AI.AboutTypeID = @AboutTypeID
                            AND AI.ReferenceID = HR.HR_REP_ID
                ) AttachmentCount ,
                ( SELECT    COUNT(*)
                  FROM      dbo.VendorClientAgreements (NOLOCK)
                  WHERE     ProjectID = HR.HR_REP_ID
                ) VendorCount ,
                ( SELECT    COUNT(*)
                  FROM      dbo.VendorProjectsPricing (NOLOCK)
                  WHERE     ProjectID = HR.HR_REP_ID
                ) ProjectPricingCount ,
                ( SELECT    COUNT(*)
                  FROM      dbo.WorkHistory (NOLOCK)
                  WHERE     ReferenceID = HR.HR_REP_ID
														AND AboutTypeID = @AboutTypeID
                ) WorkHistoryCount
                , dbo.fnConcatenateRecruiterDepartmentIDs (HR.UserID, 0) AS RecruiterDepartments
                , dbo.fnConcatenateRecruiterDepartmentIDs (HR.UserID, 1) AS RecruiterDefaultDepartments
                , HR.UserID
				, U.RequiresFederation
        FROM    dbo.HR_REPRESENTATIVE HR ( NOLOCK )
                LEFT JOIN dbo.Vendors V ( NOLOCK ) ON HR.EntityID = V.EntityID
                                                      AND v.IsEnabled = 1
                LEFT JOIN dbo.Addresses DefaultAddress ( NOLOCK ) ON HR.DefaultAddressID = DefaultAddress.AddressID
                LEFT JOIN dbo.States DefaultAddressState ( NOLOCK ) ON DefaultAddress.StateID = DefaultAddressState.StateID
                LEFT JOIN dbo.Countries DefaultAddressCountry ( NOLOCK ) ON DefaultAddress.CountryID = DefaultAddressCountry.CountryID
                LEFT JOIN dbo.Regions DefaultAddressRegion ( NOLOCK ) ON DefaultAddress.RegionID = DefaultAddressRegion.RegionID
				INNER JOIN dbo.aspnet_Users U (NOLOCK) ON U.UserId = HR.UserID
                OUTER APPLY ( SELECT TOP 1
                                        RI.NoteID LastNoteID ,
                                        NI.Body LastNoteBody ,
                                        NI.CreatedBy LastNoteCreatedBy ,
                                        NI.CreatedOn LastNoteCreatedOn ,
                                        COALESCE(HRI.FIRST_NAME, '') + ' ' + COALESCE(HRI.LAST_NAME, '') LastNoteCreatedByName
                              FROM      dbo.NoteReferences RI ( NOLOCK )
                                        JOIN dbo.NewNotes NI ( NOLOCK ) ON RI.NoteID = NI.NoteID
                                        JOIN dbo.HR_REPRESENTATIVE HRI ( NOLOCK ) ON NI.CreatedBy = HRI.UserID
                              WHERE     RI.AboutTypeID = @AboutTypeID
                                        AND RI.ReferenceID = HR.HR_REP_ID
                              ORDER BY  NI.CreatedOn DESC ,
                                        RI.NoteID DESC
                            ) LastNote
                OUTER APPLY ( SELECT TOP 1
                                        RI.NoteID LastNoteIDByUser ,
                                        NI.Body LastNoteBodyByUser ,
                                        NI.CreatedBy ,
                                        NI.CreatedOn LastNoteCreatedOnByUser ,
                                        RI.RecordID
                              FROM      dbo.NoteReferences RI ( NOLOCK )
                                        JOIN dbo.NewNotes NI ( NOLOCK ) ON RI.NoteID = NI.NoteID
                              WHERE     RI.AboutTypeID = @AboutTypeID
                                        AND RI.ReferenceID = HR.HR_REP_ID
                                        AND NI.CreatedBy = @UserID
                              ORDER BY  NI.CreatedOn DESC ,
                                        RI.NoteID DESC
                            ) LastNoteByUser
        WHERE   HR.HR_REP_ID = @ReferenceID

		OPTION(OPTIMIZE FOR UNKNOWN);
    END


GO

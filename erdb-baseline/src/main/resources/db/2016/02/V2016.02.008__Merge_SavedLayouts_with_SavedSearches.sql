﻿/*
	This script is in relation to DEV-7751 - "merge saved searches and saved layouts in one".
	It drops dbo.SavedLayouts and transfers all data to dbo.SavedSearches.
*/

-- Drop these functions just in case they were left over from the last run somehow.
if object_id('dbo.__parseLayoutName') is not null drop function dbo.__parseLayoutName
if object_id('dbo.__parseLayout') is not null drop function dbo.__parseLayout
GO

/*
	The following two functions parse Name and definition (i.e. columns, sorters, groupings) from a layout JSON.
	The layout JSON has the following shape:

		{
			"Name": "some name",
			"Layout": {
				Columns: ...
				Sorters: ...
				WhateverElse: ...
			}
		}

	The contents of the "Layout" section don't matter very much, we just want to transfer them verbatim to the
	new storage place (i.e. dbo.SavedSearches). But we need to extract "Name" and put it in as a separate column.
	It is a historical accident that both "Name" and "Layout" ended up as one JSON in the column. An oversight.

	To parse out the Name (in dbo.__parseLayoutName) is relatively simple: just find the prefix "Name":, and then
	find the nearest double quote, then cut out the text between them. For this to work when double quotes present
	inside the string itself, we replace them with single quotes.

	To parse out the Layout (in dbo.__parseLayout), we find the prefix "Layout":, and from there go through the
	string sequentially, counting opening and closing curly braces. When we find the last closing curly brace, that's
	the end of the JSON object, so we cut out that portion.
*/

create function dbo.__parseLayoutName( @json as nvarchar(max) ) returns nvarchar(max) as begin
	set @json = replace(@json, '\"', '''')
	declare @nameIndex int = PATINDEX('%"Name":"%', @json) + 8
	declare @nameLength int = patindex('%"%', substring(@json,@nameIndex,len(@json)-@nameIndex)) - 1
	return substring(@json, @nameIndex, @nameLength)
end
GO

create function dbo.__parseLayout( @json as nvarchar(max) ) returns nvarchar(max) as begin
	set @json = replace(@json, '\"', '''')
	declare @layoutIndex int = PATINDEX('%"Layout":{%', @json) + 9
	declare @layoutLength int = len(@json) - @layoutIndex
	declare @curlyBraceCount int = 1
	declare @i int = @layoutIndex
	while @i < len(@json) begin
		declare @c char(1) = substring(@json, @i, 1)
	
		if (@c = '{') set @curlyBraceCount = @curlyBraceCount+1
		else if (@c = '}') set @curlyBraceCount = @curlyBraceCount-1

		if (@curlyBraceCount = 0) begin set @layoutLength = @i - @layoutIndex + 1; break; end
		set @i = @i + 1
	end

	return substring(@json, @layoutIndex, @layoutLength)
end
GO

if object_id('dbo.SavedLayouts') is not null begin

	-- First, transfer the layouts themselves, inserting them in dbo.SavedSearches as "layout-only" saved search.
	-- Also transfer the original LayoutID, put it in the `FromReferenceID` column. We'll need this ID later.
	insert into dbo.SavedSearches(
		FromAboutTypeID, FromReferenceID, ToAboutTypeID, 
		SearchedBy, SearchedOn, 
		QueryString, 
		EntityID, 
		Name, 
		SearchDefinitionID, 
		IsAutomatic, IsPublic, IsDeleted, 
		LayoutJson)
	select 
		0, cast(LayoutID as nvarchar(50)), 0, 
		OwnerID, getdate(), 
		'', 
		-- dbo.SavedLayouts.EntityID, as it turns out, is nullable, and is indeed frequently left null. So we need to have a backup, and a backup for the backup.
		isnull( isnull( sl.EntityID, (select top 1 ue.EntityID from dbo.UsersInEntityRole ue where sl.OwnerID = ue.UserID) ), '00000000-0000-0000-0000-000000000E01' ), 
		dbo.__parseLayoutName(LayoutDefinition), 
		SearchDefinitionID, 
		0, 0, 0, 
		dbo.__parseLayout(LayoutDefinition)
	from dbo.SavedLayouts sl

	-- Then, don't forget to transfer the sharing information, if any some of these layouts have been shared.
	-- Here, we pick out SharingDefinitions that relate to the former SavedLayouts (hence sh.ReferenceID = ss.FromReferenceID),
	-- and _not_ targeted at SavedSearches (which they wouldn't be due to a bug, see comment in 0675_SharginDefinitions_Cleanup.sql).
	-- and then set their AboutType correctly to 1005, and set their ReferenceID to whatever their new ID in dbo.SavedSearches is.
	update sh
	set AboutType = 1005 /* SavedSearch */, ReferenceID = ss.SearchID
	from dbo.SharingDefinitions sh
	join dbo.SavedSearches ss on sh.ReferenceID = cast(ss.FromReferenceID as int) and sh.AboutType <> 1005
	-- Join with Layouts just to make sure that there is no stale FromReferenceID somewhere that just happens to match one of the IDs
	join dbo.SavedLayouts sl on sl.LayoutID = sh.ReferenceID 

	-- Clean out FromReferenceID (just in case; one has to always cleanup after oneself)
	update dbo.SavedSearches set FromReferenceID = '' where cast(FromReferenceID as int) in (select LayoutID from dbo.SavedLayouts)

	-- And once all that is done, drop the old layouts table
	drop table dbo.SavedLayouts
end
GO

-- Clean up
if object_id('dbo.__parseLayoutName') is not null drop function dbo.__parseLayoutName
if object_id('dbo.__parseLayout') is not null drop function dbo.__parseLayout
GO
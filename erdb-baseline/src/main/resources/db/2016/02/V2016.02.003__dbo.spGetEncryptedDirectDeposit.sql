﻿IF EXISTS(SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID('[dbo].[spGetEncryptedDirectDeposit]'))
EXECUTE('DROP PROCEDURE [dbo].[spGetEncryptedDirectDeposit]')

PRINT 'Creating/Updating PROCEDURE [dbo].[spGetEncryptedDirectDeposit]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Stephen Caldwell
-- Create date: 11/11/2013
-- Description:	Encrypts direct deposit information
-- =============================================
CREATE PROCEDURE [dbo].[spGetEncryptedDirectDeposit]
	-- Add the parameters for the stored procedure here
	@CandidateID int,
	@DirectDepositField1 varchar(25),
	@DirectDepositField2 varchar(15),
	@DirectDepositField3 varchar(25)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	OPEN SYMMETRIC KEY DirectDeposit_Key DECRYPTION BY CERTIFICATE CandidateDirectDeposit
    -- Insert statements for procedure here
	SELECT ENCRYPTBYKEY(KEY_GUID('DirectDeposit_Key'), @DirectDepositField1, 1, CONVERT(varbinary, @CandidateID)) as DirectDepositField1,
			 ENCRYPTBYKEY(KEY_GUID('DirectDeposit_Key'), @DirectDepositField2, 1, CONVERT(varbinary, @CandidateID)) as DirectDepositField2,
			 ENCRYPTBYKEY(KEY_GUID('DirectDeposit_Key'), @DirectDepositField3, 1, CONVERT(varbinary, @CandidateID)) as DirectDepositField3
END
GO

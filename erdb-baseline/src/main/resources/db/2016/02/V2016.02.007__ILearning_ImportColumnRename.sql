﻿-- iLearning import file column rename 'SomeCode' -> 'CATI'
IF (EXISTS (SELECT TOP 1 * FROM [dbo].[ImportItemMemberFileColumnMapping] WHERE [ImportItemMemberFileColumnMappingID] = 132 AND [ColumnName] <> N'CATI'))
BEGIN
	UPDATE [dbo].[ImportItemMemberFileColumnMapping] SET [ColumnName] = N'CATI' WHERE [ImportItemMemberFileColumnMappingID] = 132
END
-- iLearning import file column rename 'GreenProjCode' -> 'SCAProjectCode'
IF (EXISTS (SELECT TOP 1 * FROM [dbo].[ImportItemMemberFileColumnMapping] WHERE [ImportItemMemberFileColumnMappingID] = 170 AND [ColumnName] <> N'SCAProjectCode'))
BEGIN
	UPDATE [dbo].[ImportItemMemberFileColumnMapping] SET [ColumnName] = N'SCAProjectCode' WHERE [ImportItemMemberFileColumnMappingID] = 170
END
GO

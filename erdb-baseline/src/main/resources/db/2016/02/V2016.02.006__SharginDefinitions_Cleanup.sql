﻿/*
	The only thing we're sharing right now is "saved searches". The about type ID for saved search is 1005.
	Therefore, one would expect to see only 1005 in the SharingDefinitions.AboutType column.

	But this is not the case. Due to a bug in initial implementation, that value was being set not to 1005,
	but to whatever about type the saved search was targeted at - candidate, company, position, etc.
	
	The bug that was causing this has been fixed. This script is intended to rectify the damage already done.
*/
update dbo.SharingDefinitions set AboutType=1005 --SavedSearch
where ReferenceID in (select SearchID from SavedSearches)

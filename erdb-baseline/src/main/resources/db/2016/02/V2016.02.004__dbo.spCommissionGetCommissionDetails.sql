﻿IF EXISTS(SELECT * FROM	dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[spCommissionGetCommissionDetails]'))
EXECUTE('DROP PROCEDURE [dbo].[spCommissionGetCommissionDetails]')

PRINT 'Creating/Updating PROCEDURE [dbo].[spCommissionGetCommissionDetails]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TD
-- Create date: 2013.06.26
-- Description:	Return details for the commission detail report
-- =============================================
CREATE PROCEDURE [dbo].[spCommissionGetCommissionDetails]
	@EntityID uniqueidentifier,
	@CommissionID int = null,
	@RecruiterIDs varchar(max) = null,
	@FormulaIDs varchar(max) = null,
	@StartDate datetime = null,
	@EndDate datetime = null,
	@GroupBy varchar(50) = null,
	@IncludeAdjustments bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/* parse delimited input parameters into temporary tables */
CREATE TABLE #RecruiterID (RecruiterID INT NOT NULL PRIMARY KEY CLUSTERED)
CREATE TABLE #FormulaID (FormulaID INT NOT NULL PRIMARY KEY CLUSTERED)

INSERT INTO #FormulaID (FormulaID)
SELECT DISTINCT CONVERT(INT, Value) AS FormulaID
FROM dbo.Split(@FormulaIDs, ',')
WHERE Value IS NOT NULL

INSERT INTO #RecruiterID (RecruiterID)
SELECT DISTINCT CONVERT(INT, Value) AS RecruiterID
FROM dbo.Split(@RecruiterIDs, ',')
WHERE Value IS NOT NULL

	-- Insert statements for procedure here
	IF @CommissionID IS NULL
	BEGIN
		IF @GroupBy IS NULL
		BEGIN
			SELECT
				C.CommissionID,
				C.FeeCredited as TotalFeeCredited,
				C.CommissionCredited as TotalCommissionCredited,
				C.CommissionApplied as TotalCommissionApplied,
				C.DrawApplied as TotalDrawApplied,
				C.CreatedOn,
				C.CreatedBy,
				C.RangePeriodID,
				C.EffectiveDate,
				C.IsPaid,
				RangeCal.StartDate,
				RangeCal.EndDate,
				HR.FIRST_NAME as FirstName,
				HR.LAST_NAME as LastName,
				isnull(HR.FIRST_NAME, '') + ' ' + isnull(HR.LAST_NAME, '') as RecruiterName,
				HR.ExternalPayrollID,
				CF.Name as FormulaName,
				isnull(CD.FeeCredited,C.FeeCredited) as FeeCredited,
				isnull(CD.CommissionCredited, C.CommissionCredited) as CommissionCredited, 
				CASE isnull(CD.FeeCredited, C.FeeCredited) WHEN 0 THEN 0 ELSE isnull(CD.CommissionCredited, C.CommissionCredited) / isnull(CD.FeeCredited, C.FeeCredited) END as CommissionPercent,
				TAmounts.TotalHours as TSHours,
				TAmounts.Billed as TSBilled,
				TAmounts.Paid as TSPaid,
				TAmounts.Admin as TSAdmin,
				TAmounts.TaxEstimated as TSTaxEstimated,
				TAmounts.WorkComp as TSWorkComp,
				TAmounts.ExpenseBilled as TSExpenseBilled,
				TAmounts.ExpensePaid as TSExpensePaid,
				TAmounts.ExpenseLoad as TSExpenseLoad,
				TAmounts.TotalLoad as TSTotalLoad,
				TAmounts.VMSBurden as TSVMSBurden,
				TAmounts.RequirementsBurden as TSReqBurden,
				TAmounts.Margin as TSMargin,
				TAmounts.Profit as TSProfit,
				TAmounts.MatchRatesBurdenAmount as TSRateBurden,
				MT.TIMESHEET_ID as TimesheetID,
				MT.MATCH_ID as MatchID,
				Cand.CANDIDATE_ID as CandidateID,
				COALESCE(Cand.FIRST_NAME, '') + ' ' + COALESCE(Cand.LAST_NAME, '') as CandidateName,
				Pos.POSITION_ID as PositionID,
				Pos.POSITION_TITLE as PositionTitle,
				MTD.DESCRIPTION as TransactionDesc,
				CD.Note as CommissionDetailNote,
				Comp.COMPANY_ID as CompanyID,
				Comp.NAME as CompanyName
			FROM
				dbo.Commission C
				LEFT OUTER JOIN dbo.RecruiterCommissionFormula RCF
					ON C.RecruiterCommissionFormulaID = RCF.RecruiterCommissionFormulaID
				LEFT OUTER JOIN dbo.CommissionFormula CF
					ON RCF.CommissionFormulaID = CF.CommissionFormulaID
				LEFT OUTER JOIN dbo.RecruiterCommissionPlan RCP
					ON C.RecruiterCommissionPlanID = RCP.RecruiterCommissionPlanID
				LEFT OUTER JOIN dbo.HR_REPRESENTATIVE HR
					ON RCP.RecruiterID = HR.UserID
				LEFT OUTER JOIN dbo.RangeCalendarPeriod RangeCal
					ON C.RangePeriodID = RangeCal.RangePeriodID
				LEFT OUTER JOIN dbo.CommissionDetail CD
					ON C.CommissionID = CD.CommissionID
				LEFT OUTER JOIN dbo.MATCH_TRANS MT
					ON MT.TRANS_ID = CD.TransID
				LEFT OUTER JOIN dbo.MATCH_TRANS_DESC MTD
					ON MT.TYPE_ID = MTD.TYPE_ID
				LEFT OUTER JOIN dbo.Match M
					ON MT.MATCH_ID = M.MATCH_ID
				LEFT OUTER JOIN dbo.CANDIDATE Cand
					ON M.CANDIDATE_ID = Cand.CANDIDATE_ID
				LEFT OUTER JOIN dbo.POSITION Pos
					ON M.POSITION_ID = Pos.POSITION_ID
				LEFT OUTER JOIN dbo.COMPANY Comp
					ON Pos.COMPANY_ID = Comp.COMPANY_ID
				LEFT OUTER JOIN dbo.vwGetExistingTimesheetAmountsForTransaction TAmounts 
					ON MT.TRANS_ID = TAmounts.TransactionID
			WHERE
				(RCF.CommissionFormulaID IN (SELECT FormulaID from #FormulaID) OR (@IncludeAdjustments = 1 AND C.RecruiterCommissionFormulaID IS NULL))
				AND C.EffectiveDate >= @StartDate
				AND C.EffectiveDate <= @EndDate
				AND (@RecruiterIDs is null or (HR.HR_REP_ID IN (SELECT RecruiterID from #RecruiterID)))
			ORDER BY
				HR.HR_REP_ID,
				CF.Name,
				C.EffectiveDate
		END
		ELSE
		BEGIN
			SELECT
				MAX(C.CommissionID) AS CommissionID,
				SUM(C.FeeCredited) as TotalFeeCredited,
				SUM(C.CommissionCredited) as TotalCommissionCredited,
				SUM(C.CommissionApplied) as TotalCommissionApplied,
				SUM(C.DrawApplied) as TotalDrawApplied,
				MAX(C.CreatedOn) AS CreatedOn,
				MAX(C.RangePeriodID) AS RangePeriodID,
				C.EffectiveDate,
				CAST(MAX(CAST(C.IsPaid as INT)) AS BIT) as IsPaid,
				MAX(RangeCal.StartDate) AS StartDate,
				MAX(RangeCal.EndDate) AS EndDate,
				MAX(HR.FIRST_NAME) as FirstName,
				MAX(HR.LAST_NAME) as LastName,
				MAX(isnull(HR.FIRST_NAME, '')) + ' ' + MAX(isnull(HR.LAST_NAME, '')) as RecruiterName,
				MAX(HR.ExternalPayrollID) as ExternalPayrollID,
				MAX(CF.Name) as FormulaName,
				SUM(isnull(CD.FeeCredited, C.FeeCredited)) as FeeCredited,
				SUM(isnull(CD.CommissionCredited, C.CommissionCredited)) AS CommissionCredited,
				CASE MAX(CD.FeeCredited) WHEN 0 THEN 0 ELSE MAX(CD.CommissionCredited) / MAX(CD.FeeCredited) END as CommissionPercent,
				SUM(TAmounts.TotalHours) as TSHours,
				SUM(TAmounts.Billed) as TSBilled,
				SUM(TAmounts.Paid) as TSPaid,
				SUM(TAmounts.Admin) as TSAdmin,
				SUM(TAmounts.TaxEstimated) as TSTaxEstimated,
				SUM(TAmounts.WorkComp) as TSWorkComp,
				SUM(TAmounts.ExpenseBilled) as TSExpenseBilled,
				SUM(TAmounts.ExpensePaid) as TSExpensePaid,
				SUM(TAmounts.ExpenseLoad) as TSExpenseLoad,
				SUM(TAmounts.TotalLoad) as TSTotalLoad,
				SUM(TAmounts.VMSBurden) as TSVMSBurden,
				SUM(TAmounts.RequirementsBurden) as TSReqBurden,
				SUM(TAmounts.Margin) as TSMargin,
				SUM(TAmounts.Profit) as TSProfit,
				SUM(TAmounts.MatchRatesBurdenAmount) as TSRateBurden,
				MAX(MT.TIMESHEET_ID) as TimesheetID,
				MAX(MT.MATCH_ID) as MatchID,
				MAX(Cand.CANDIDATE_ID) as CandidateID,
				MAX(Cand.FIRST_NAME) + ' ' + MAX(Cand.LAST_NAME) as CandidateName,
				MAX(Pos.POSITION_ID) as PositionID,
				MAX(Pos.POSITION_TITLE) as PositionTitle,
				MAX(MTD.DESCRIPTION) as TransactionDesc,
				MAX(CD.Note) as CommissionDetailNote,
				MAX(Comp.COMPANY_ID) as CompanyID,
				MAX(Comp.NAME) as CompanyName
			FROM
				dbo.Commission C
				LEFT OUTER JOIN dbo.RecruiterCommissionFormula RCF
					ON C.RecruiterCommissionFormulaID = RCF.RecruiterCommissionFormulaID
				LEFT OUTER JOIN dbo.CommissionFormula CF
					ON RCF.CommissionFormulaID = CF.CommissionFormulaID
				LEFT OUTER JOIN dbo.RecruiterCommissionPlan RCP
					ON C.RecruiterCommissionPlanID = RCP.RecruiterCommissionPlanID
				LEFT OUTER JOIN dbo.HR_REPRESENTATIVE HR
					ON RCP.RecruiterID = HR.UserID
				LEFT OUTER JOIN dbo.RangeCalendarPeriod RangeCal
					ON C.RangePeriodID = RangeCal.RangePeriodID
				LEFT OUTER JOIN dbo.CommissionDetail CD
					ON C.CommissionID = CD.CommissionID
				LEFT OUTER JOIN dbo.MATCH_TRANS MT
					ON MT.TRANS_ID = CD.TransID
				LEFT OUTER JOIN dbo.MATCH_TRANS_DESC MTD
					ON MT.TYPE_ID = MTD.TYPE_ID
				LEFT OUTER JOIN dbo.Match M
					ON MT.MATCH_ID = M.MATCH_ID
				LEFT OUTER JOIN dbo.CANDIDATE Cand
					ON M.CANDIDATE_ID = Cand.CANDIDATE_ID
				LEFT OUTER JOIN dbo.POSITION Pos
					ON M.POSITION_ID = Pos.POSITION_ID
				LEFT OUTER JOIN dbo.COMPANY Comp
					ON Pos.COMPANY_ID = Comp.COMPANY_ID
				LEFT OUTER JOIN dbo.vwGetExistingTimesheetAmountsForTransaction TAmounts 
					ON MT.TRANS_ID = TAmounts.TransactionID
			WHERE
				(RCF.CommissionFormulaID IN (SELECT FormulaID from #FormulaID) OR (@IncludeAdjustments = 1 AND C.RecruiterCommissionFormulaID IS NULL))
				AND C.EffectiveDate >= @StartDate
				AND C.EffectiveDate <= @EndDate
				AND (@RecruiterIDs is null or (HR.HR_REP_ID IN (SELECT RecruiterID from #RecruiterID)))
			GROUP BY  HR.HR_REP_ID, C.EffectiveDate, c.IsPaid
			ORDER BY    HR.HR_REP_ID, C.EffectiveDate
		END
	END
	ELSE
	BEGIN
		SELECT
			C.CommissionID,
			C.FeeCredited as TotalFeeCredited,
			C.CommissionCredited as TotalCommissionCredited,
			C.CommissionApplied as TotalCommissionApplied,
			C.DrawApplied as TotalDrawApplied,
			C.CreatedOn,
			C.CreatedBy,
			C.RangePeriodID,
			C.EffectiveDate,
			C.IsPaid,
			RangeCal.StartDate, RangeCal.EndDate,
			HR.FIRST_NAME as FirstName, HR.LAST_NAME as LastName, isnull(HR.FIRST_NAME, '') + ' ' + isnull(HR.LAST_NAME, '') as RecruiterName,
			CF.Name as FormulaName,
			isnull(CD.FeeCredited,C.FeeCredited) as FeeCredited, isnull(CD.CommissionCredited, C.CommissionCredited) as CommissionCredited, 
			CASE isnull(CD.FeeCredited, C.FeeCredited) WHEN 0 THEN 0 ELSE isnull(CD.CommissionCredited, C.CommissionCredited) / isnull(CD.FeeCredited, C.FeeCredited) END as CommissionPercent,
			TAmounts.TotalHours as TSHours, TAmounts.Billed as TSBilled, TAmounts.Paid as TSPaid, TAmounts.Admin as TSAdmin, TAmounts.TaxEstimated as TSTaxEstimated,
			TAmounts.WorkComp as TSWorkComp, TAmounts.ExpenseBilled as TSExpenseBilled, TAmounts.ExpensePaid as TSExpensePaid,
			TAmounts.ExpenseLoad as TSExpenseLoad, TAmounts.TotalLoad as TSTotalLoad, TAmounts.VMSBurden as TSVMSBurden, TAmounts.RequirementsBurden as TSReqBurden,
			TAmounts.Margin as TSMargin, TAmounts.Profit as TSProfit, TAmounts.MatchRatesBurdenAmount as TSRateBurden,
			MT.TIMESHEET_ID as TimesheetID, MT.MATCH_ID as MatchID,
			Cand.CANDIDATE_ID as CandidateID, COALESCE(Cand.FIRST_NAME, '') + ' ' + COALESCE(Cand.LAST_NAME, '') as CandidateName,
			Pos.POSITION_ID as PositionID, Pos.POSITION_TITLE as PositionTitle,
			MTD.DESCRIPTION as TransactionDesc, CD.Note as CommissionDetailNote,
			Comp.COMPANY_ID as CompanyID, Comp.NAME as CompanyName
		FROM
			dbo.Commission C
			LEFT OUTER JOIN dbo.RecruiterCommissionFormula RCF
				ON C.RecruiterCommissionFormulaID = RCF.RecruiterCommissionFormulaID
			LEFT OUTER JOIN dbo.CommissionFormula CF
				ON RCF.CommissionFormulaID = CF.CommissionFormulaID
			LEFT OUTER JOIN dbo.RecruiterCommissionPlan RCP
				ON RCF.RecruiterCommissionPlanID = RCP.RecruiterCommissionPlanID
			LEFT OUTER JOIN dbo.HR_REPRESENTATIVE HR
				ON RCP.RecruiterID = HR.UserID
			LEFT OUTER JOIN dbo.RangeCalendarPeriod RangeCal
				ON C.RangePeriodID = RangeCal.RangePeriodID
			LEFT OUTER JOIN dbo.CommissionDetail CD
				ON C.CommissionID = CD.CommissionID
			LEFT OUTER JOIN dbo.MATCH_TRANS MT
				ON MT.TRANS_ID = CD.TransID
			LEFT OUTER JOIN dbo.MATCH_TRANS_DESC MTD
				ON MT.TYPE_ID = MTD.TYPE_ID
			LEFT OUTER JOIN dbo.Match M
				ON MT.MATCH_ID = M.MATCH_ID
			LEFT OUTER JOIN dbo.CANDIDATE Cand
				ON M.CANDIDATE_ID = Cand.CANDIDATE_ID
			LEFT OUTER JOIN dbo.POSITION Pos
				ON M.POSITION_ID = Pos.POSITION_ID
			LEFT OUTER JOIN dbo.COMPANY Comp
				ON Pos.COMPANY_ID = Comp.COMPANY_ID
			LEFT OUTER JOIN dbo.vwGetExistingTimesheetAmountsForTransaction TAmounts 
				ON MT.TRANS_ID = TAmounts.TransactionID
		WHERE C.CommissionID = @CommissionID
		ORDER BY
			HR.HR_REP_ID,
			CF.Name,
			C.EffectiveDate
	END
END
GO

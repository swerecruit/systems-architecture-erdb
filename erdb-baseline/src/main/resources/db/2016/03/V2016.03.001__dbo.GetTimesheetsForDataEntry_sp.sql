﻿IF EXISTS(SELECT * FROM	dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[GetTimesheetsForDataEntry_sp]'))
EXECUTE('DROP PROCEDURE [dbo].[GetTimesheetsForDataEntry_sp]')

PRINT 'Creating/Updating PROCEDURE [dbo].[GetTimesheetsForDataEntry_sp]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetTimesheetsForDataEntry_sp]
	@PayPeriodEndDate DATE,
	@BatchID INT = NULL,
	@EntityID UNIQUEIDENTIFIER,
	@IncludeUnprocessedW2 BIT = 0,
	@IncludeUnprocessed1099 BIT = 0,
	@IncludeW2 BIT = 1,
	@Include1099 BIT = 1
AS
	/*
	Create a structure out of DEPT_CREDT that has unique MATCH_ID's with DEPT Names concatenated by ;
	deliberately stuffing this into Temp table as opposed to table variable as this can be relatively large structure
	CP			01/15/2013	-Create #TimeSheet to keep filtered partial records, then join #TimeSheet with rest tables to get all fields
	CP			03/05/2013	use temp table to replace function:GetTimesheetAounts_fn
	BMoore		11/06/2013  eliminate usage of UDF's, this was performing so badly
	HJ			03/17/2014  totally rewrite.
	*/
	---------------------------------------------------------------------
	-- Declarations
	---------------------------------------------------------------------
	SET NOCOUNT ON

	-- will take this out but better here than NOLOCK at individual level
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE -- standard variables
		@ErrorMsg varchar(1000),
		@ErrorSeverity int,
		@ExitCode int; -- Normally 0 means success and -1 failure. Set @ExitCode to a positive number to return a custom status.

		CREATE TABLE #dep
		(
			MATCH_ID INT NOT NULL,
			DEPTS VARCHAR(MAX)
		)

	---------------------------------------------------------------------
	-- initialize variables
	---------------------------------------------------------------------

	SELECT   -- standard variables
		@ExitCode         = 0                 -- success is the default

	BEGIN TRY
		SELECT
			t.TimesheetID,
			t.MatchID,
			t.StartDate AS StartDate,
			t.EndDate AS EndDate,
			t.PayPeriodEndDate,
			t.IsExported,
			t.TaxEstimated,
			t.IsAdjustment,
			t.Admin,
			t.WorkComp,
			t.Billed,
			t.ApprovedDate,
			t.SubmittedDate,
			t.ApprovedBy,
			t.SubmittedBy,
			t.IsNoCount,
			t.IsPendingAdditionalApproval,
			m.match_ID,
			m.Candidate_ID,
			m.CNTR_WORK_COMP_ID,
			m.OvertimeType,
			m.AccountingClassID,
			m.AccountinglFieldValue1,
			m.AccountinglFieldValue2,
			m.AccountinglFieldValue3,
			m.AccountinglFieldValue4,
			m.START_DATE,
			m.CNTR_END_DATE,
			m.NotesToBackOffice,
			m.CONTACT_ID_BILLING,
			m.PENDING_STATUS,
			m.CNTR_TAX_ID,
			m.AccountExecID,
			m.CurrentCandidateOwnerID,
			m.VMSBurdenID,
			comp.COMPANY_ID,
			comp.Name,
			p.POSITION_ID,
			p.POSITION_TITLE,
			DefaultPositionAddress.Street1 PositionStreet1,
			p.Contact_ID,
			tbd.Name AS BatchName,
			(
				SELECT COUNT(*)
				FROM dbo.Attachments a
				WHERE ((a.AboutTypeID = 12 AND a.ReferenceID = t.TimesheetID)
					OR (a.AboutTypeID = 1 AND a.ReferenceID = comp.COMPANY_ID)
					OR (a.AboutTypeID = 4 AND a.ReferenceID = p.POSITION_ID)
					OR (a.AboutTypeID = 5 AND a.ReferenceID = m.MATCH_ID))
					AND a.IncludeWithInvoice = 1
					AND a.DeletedBy IS NULL
					AND a.ParentAttachmentID IS NULL
			) AttachmentsWithInvoiceCount,
			(
				SELECT COUNT(*)
				FROM dbo.Attachments a
				WHERE ((a.AboutTypeID = 12 AND a.ReferenceID = t.TimesheetID)
					OR (a.AboutTypeID = 1 AND a.ReferenceID = comp.COMPANY_ID)
					OR (a.AboutTypeID = 4 AND a.ReferenceID = p.POSITION_ID)
					OR (a.AboutTypeID = 5 AND a.ReferenceID = m.MATCH_ID))
					AND a.IncludeForReview = 1
					AND a.DeletedBy IS NULL
					AND a.ParentAttachmentID IS NULL
			) AttachmentsForReviewCount,
			(SELECT COUNT(*) 
				FROM dbo.NewNotes NI
				INNER JOIN  dbo.NoteReferences nr ON nr.NoteID = NI.NoteID
                            JOIN dbo.NoteActions NA  ON NA.ActionID = NI.ActionID
					WHERE nr.AboutTypeID = 12 AND nr.ReferenceID = t.TimesheetID  AND NA.Permission IS NULL
					GROUP BY nr.ReferenceID ) AS NotesCount,
			m.TimesheetCreationOption,
			rc.Name as TimesheetRangeCalendarName
		INTO	  #Timesheet
		FROM	  dbo.Timesheet t
			INNER JOIN dbo.Match m
				ON t.MatchID = m.MATCH_ID
			LEFT JOIN dbo.RangeCalendar rc
				ON m.TimesheetRangeCalendarID = rc.RangeCalendarID
			INNER JOIN dbo.POSITION p
				ON m.POSITION_ID = p.POSITION_ID
			LEFT OUTER JOIN dbo.COMPANY comp
				ON p.COMPANY_ID = comp.COMPANY_ID
			LEFT OUTER JOIN dbo.Addresses DefaultPositionAddress
				ON p.DefaultAddressID = DefaultPositionAddress.AddressID
			LEFT OUTER JOIN dbo.CNTR_BURDEN_TAX tax
				ON tax.TAX_ID = m.CNTR_TAX_ID
			LEFT OUTER JOIN dbo.TimesheetBatchDefinitions tbd
				ON tbd.BatchID = m.TimesheetBatchID
		WHERE
			( @BatchID IS NULL OR @BatchID = 0 OR @BatchID = m.TimesheetBatchID	)
			AND comp.OwnerEntityID = @EntityID
			AND (@IncludeW2 = 1 OR ISNULL(tax.RequiresW2, 0) = 0)
			AND (@Include1099 = 1 OR ISNULL(tax.Requires1099, 0) = 0)
			AND (t.PayPeriodEndDate = @PayPeriodEndDate
				OR (@IncludeW2 = 1 AND @IncludeUnprocessedW2 = 1 AND t.PayPeriodEndDate < @PayPeriodEndDate AND NOT EXISTS (SELECT tlp.TimesheetLinePayID FROM dbo.TimesheetLinePay tlp INNER JOIN dbo.TimesheetLineAttribution tla ON tlp.TimesheetLinePayID = tla.TimesheetLinePayID WHERE tla.TimesheetID = t.TimesheetID AND tlp.PayrollBatchRunID IS NOT NULL))
				OR (@Include1099 = 1 AND @IncludeUnprocessed1099 = 1 AND t.PayPeriodEndDate < @PayPeriodEndDate AND NOT EXISTS (SELECT tlp.TimesheetLinePayID FROM dbo.TimesheetLinePay tlp INNER JOIN dbo.TimesheetLineAttribution tla ON tlp.TimesheetLinePayID = tla.TimesheetLinePayID WHERE tla.TimesheetID = t.TimesheetID AND tlp.VoucherBatchRunID IS NOT NULL))
			)

		SELECT DISTINCT
			td.TimesheetID,
			1 AS FieldID,
			td.LineDescriptionValue1 AS Value
		INTO #unpivoted_linevalues
		FROM dbo.TimesheetLineEntry td
			INNER JOIN #Timesheet
				ON td.TimesheetID = #Timesheet.TimesheetID
		WHERE td.LineDescriptionValue1 IS NOT NULL AND td.LineDescriptionValue1 <> ''
		UNION ALL
		SELECT DISTINCT
			td.TimesheetID,
			2 AS FieldID,
			td.LineDescriptionValue2 AS Value
		FROM dbo.TimesheetLineEntry td
			INNER JOIN #Timesheet ON td.TimesheetID = #Timesheet.TimesheetID
		WHERE	td.LineDescriptionValue2 IS NOT NULL AND td.LineDescriptionValue2 <> ''
		UNION ALL
		SELECT DISTINCT
			td.TimesheetID,
			3 AS FieldID,
			td.LineDescriptionValue3 AS Value
		FROM dbo.TimesheetLineEntry td
			INNER JOIN #Timesheet ON td.TimesheetID = #Timesheet.TimesheetID
		WHERE	td.LineDescriptionValue3 IS NOT NULL AND td.LineDescriptionValue3 <> ''	
		UNION ALL
		SELECT DISTINCT
			td.TimesheetID,
			4 AS FieldID,
			td.LineDescriptionValue4 AS Value
		FROM dbo.TimesheetLineEntry td
			INNER JOIN #Timesheet
				ON td.TimesheetID = #Timesheet.TimesheetID
		WHERE	td.LineDescriptionValue4 IS NOT NULL AND td.LineDescriptionValue4 <> ''	
	
		SELECT
			TimesheetID,
			FieldID,
			STUFF(
				(
					SELECT ', ' + Value
					FROM #unpivoted_linevalues
					WHERE (TimesheetID = lv2.TimesheetID AND FieldID = lv2.FieldID)
					FOR XML PATH ('')
				), 1, 2, '') AS Value
		INTO #normalized_linevalues
		FROM #unpivoted_linevalues lv2
		GROUP BY
			TimesheetID,
			FieldID
	
		INSERT INTO #dep
		(
			MATCH_ID ,
			DEPTS
		)
		SELECT DISTINCT
			c.MATCH_ID,
			DEPTS = LEFT(o.list, LEN(o.list) - 1)
		FROM
			dbo.DEPT_CREDIT c
			INNER JOIN #Timesheet t
				ON c.Match_ID = t.Match_ID
			CROSS APPLY ( 
				SELECT NAME + ', ' AS [text()]
				FROM dbo.Departments e
					INNER JOIN dbo.DEPT_CREDIT c2
						ON e.DepartmentID = c2.DEPT_ID
				WHERE c.MATCH_ID = c2.MATCH_ID
				ORDER BY  
					NAME
				FOR XML PATH('')
			) o ( list )
		ORDER BY
			c.MATCH_ID

	SELECT
		Timesheet.TimesheetID,
		ISNULL(SUM(CASE WHEN dbo.TimesheetTRC.DoNotCountHours = 0 and Timesheet.IsNoCount = 0 THEN tle.Hours ELSE 0 END), 0) AS [Hours],
		ISNULL(SUM(dbo.RoundBanker(tle.Hours * tle.BillRate, 2)), 0) AS Billed,
		ISNULL(SUM(dbo.RoundBanker(tle.Hours * tle.PayRate,  2)), 0) AS Paid,
		ISNULL(SUM(tle.ExpenseBilled),  0) AS ExpenseBilled,
		ISNULL(SUM(tle.ExpensePaid), 0) AS ExpensePaid,
		ISNULL(SUM(tle.ExpenseLoad), 0) AS ExpenseLoad,
		ISNULL(MIN(tle.StatusID),1) TimesheetStatusID
	INTO #TimesheetAmount
	FROM #Timesheet Timesheet
		INNER JOIN dbo.TimesheetLineEntry tle ON Timesheet.TimesheetID = tle.TimesheetID
		INNER JOIN dbo.RateDetail AS mr ON mr.DetailID = tle.RateID
		INNER JOIN dbo.TimesheetTRC ON mr.TRCID = dbo.TimesheetTRC.TRCID
	GROUP BY 
		Timesheet.TimesheetID

	SELECT
		Timesheet.TimesheetID,
		ISNULL((SELECT CAST(SUM(CASE WHEN tleac.TimesheetLineEntryID IS NULL THEN 0 ELSE 1 END) AS DECIMAL(9,2)) / CAST(COUNT(tle.TimesheetLineEntryID) AS DECIMAL(9,2))
					 FROM dbo.TimesheetLineEntry tle
					 LEFT OUTER JOIN (select distinct TimesheetLineEntryID from dbo.TimesheetLineEntryAttributionContext) tleac ON tle.TimesheetLineEntryID = tleac.TimesheetLineEntryID
					 WHERE tle.TimesheetID = Timesheet.TimesheetID
					), 0)  as ProcessedPercent,
		ISNULL((SELECT CAST(SUM(CASE WHEN tla.TimesheetLinePayID IS NULL THEN 0 ELSE 1 END) AS DECIMAL(9,2)) / CAST(COUNT(tla.TimesheetLineAttributionID) AS DECIMAL(9,2))
					 FROM dbo.TimesheetLineAttribution tla
					 WHERE tla.TimesheetID = Timesheet.TimesheetID
					 AND tla.BillQuantityFinal <> 0
					 AND tla.BillRateFinal <> 0
					), 0) as PayRecordCreatedPercent,
		ISNULL((SELECT CAST(SUM(CASE WHEN tlp.PayrollBatchRunID IS NULL THEN 0 ELSE 1 END) AS DECIMAL(9,2)) / CAST(COUNT(tla.TimesheetLineAttributionID) AS DECIMAL(9,2))
					 FROM dbo.TimesheetLineAttribution tla
					 LEFT OUTER JOIN dbo.TimesheetLinePay tlp on tla.TimesheetLinePayID = tlp.TimesheetLinePayID
					 WHERE tla.TimesheetID = Timesheet.TimesheetID
					 AND tla.BillQuantityFinal <> 0
					 AND tla.BillRateFinal <> 0
					), 0) as PayRecordExportedPercent,
		ISNULL((SELECT CAST(SUM(CASE WHEN tlp.VoucherBatchRunID IS NULL THEN 0 ELSE 1 END) AS DECIMAL(9,2)) / CAST(COUNT(tla.TimesheetLineAttributionID) AS DECIMAL(9,2))
					 FROM dbo.TimesheetLineAttribution tla
					 LEFT OUTER JOIN dbo.TimesheetLinePay tlp on tla.TimesheetLinePayID = tlp.TimesheetLinePayID
					 WHERE tla.TimesheetID = Timesheet.TimesheetID
					 AND tla.BillQuantityFinal <> 0
					 AND tla.BillRateFinal <> 0
					), 0) as VoucherExportedPercent,
		ISNULL((SELECT CAST(SUM(CASE WHEN tla.InvoiceLineID IS NULL THEN 0 ELSE 1 END)AS DECIMAL(9,2)) / CAST(COUNT(tla.TimesheetLineAttributionID) AS DECIMAL(9,2))
						FROM dbo.TimesheetLineAttribution tla
						WHERE tla.TimesheetID = Timesheet.TimesheetID
							AND tla.BillQuantityFinal <> 0
							AND tla.BillRateFinal <> 0
					), 0) InvoiceCreatedPercent,
		ISNULL((SELECT CAST(SUM(CASE WHEN i.BatchRunID IS NULL THEN 0 ELSE 1 END)AS DECIMAL(9,2)) / CAST(COUNT(tla.TimesheetLineAttributionID) AS DECIMAL(9,2))
						FROM dbo.TimesheetLineAttribution tla
						LEFT OUTER JOIN dbo.InvoiceLine il on tla.InvoiceLineID = il.InvoiceLineID
						LEFT OUTER JOIN dbo.INVOICE i on il.InvoiceID = i.INVOICE_ID
						WHERE tla.TimesheetID = Timesheet.TimesheetID
							AND tla.BillQuantityFinal <> 0
							AND tla.BillRateFinal <> 0
					), 0) InvoiceExportedPercent,
		ISNULL((SELECT CAST(SUM(CASE WHEN tle.StatusID >= 3 THEN 1 ELSE 0 END) AS DECIMAL(9,2)) / CAST(COUNT(tle.TimesheetLineEntryID) AS DECIMAL(9,2))
						FROM dbo.TimesheetLineEntry tle
						WHERE tle.TimesheetID = Timesheet.TimesheetID
					), 0) ApprovedPercent,
		ISNULL((SELECT CAST(SUM(CASE WHEN tla.WipExportBatchRunID IS NULL THEN 0 ELSE 1 END) AS DECIMAL(9,2)) / CAST(COUNT(tla.TimesheetLineAttributionID) AS DECIMAL(9,2))
					 FROM dbo.TimesheetLineAttribution tla
					 WHERE tla.TimesheetID = Timesheet.TimesheetID
					 AND tla.PayQuantityFinal <> 0
					 AND tla.PayRateFinal <> 0
					), 0) as WipPostedPercent,		
		(SELECT MAX(br.StartedAt)
					 FROM dbo.TimesheetLineAttribution tla
					 LEFT OUTER JOIN dbo.TimesheetLinePay tlp on tla.TimesheetLinePayID = tlp.TimesheetLinePayID
					 LEFT OUTER JOIN dbo.BatchRuns br on tlp.PayrollBatchRunID = br.BatchRunID
					 WHERE tla.TimesheetID = Timesheet.TimesheetID
					 AND tla.BillQuantityFinal <> 0
					 AND tla.BillRateFinal <> 0
					 AND br.StartedAt IS NOT NULL
					) as PayRecordExportedDate
	INTO #TimesheetProcessing
	FROM #Timesheet Timesheet
	GROUP BY 
		Timesheet.TimesheetID

	/* preliminary Timesheet Approvers data, building block to replace former expensive UDF usage */			
	-- 1) Approved By Contacts from the Match or Company
	SELECT
		T.TimesheetID, 
		m.CONTACT_ID_APPROVETIMESHEET AS Contact_ID, 
		isnull(C.FIRST_NAME, '') + ' ' + ISNULL(C.LAST_NAME, '') AS Contact
	INTO #TimesheetApprovers
	FROM #Timesheet T
		INNER JOIN dbo.Match M ON M.MATCH_ID = T.MatchID
		INNER JOIN dbo.CONTACT C ON M.CONTACT_ID_APPROVETIMESHEET = C.CONTACT_ID
	UNION
	SELECT
		T.TimesheetID,
		m.CONTACT_ID_APPROVETIMESHEET2 AS Contact_ID, 
		isnull(C.FIRST_NAME, '') + ' ' + ISNULL(C.LAST_NAME, '') AS Contact
	FROM #Timesheet T
		INNER JOIN dbo.Match M ON M.MATCH_ID = T.MatchID
		INNER JOIN dbo.CONTACT C ON M.CONTACT_ID_APPROVETIMESHEET2 = C.CONTACT_ID
	UNION
	-- 2) Approver Contacts from the timesheet days linked to Match Rates
	SELECT DISTINCT 
		T.TimesheetID,
		C.CONTACT_ID, 
		isnull(C.FIRST_NAME, '') + ' ' + ISNULL(C.LAST_NAME, '') AS Contact
	FROM	#Timesheet T
		INNER JOIN dbo.Match M ON M.MATCH_ID = T.MatchID
		INNER JOIN dbo.TimesheetLineEntry TD ON T.TimesheetID = TD.TimesheetID 
		INNER JOIN dbo.RateDetail mr ON td.RateID = mr.DetailID	
		INNER JOIN dbo.CONTACT C ON mr.ApprovalContactID = C.CONTACT_ID
	UNION
	-- 3) Approver Contacts from the timesheet days linked to Company/Facility Departments
	SELECT DISTINCT 
			T.TimesheetID,
			C.CONTACT_ID, 
			isnull(C.FIRST_NAME, '') + ' ' + ISNULL(C.LAST_NAME, '') AS Contact
	FROM	#Timesheet T
			INNER JOIN dbo.TimesheetLineEntry TD ON T.TimesheetID = TD.TimesheetID 
			INNER JOIN dbo.CompanyDepartments CD ON TD.FacilityDepartmentID = CD.CompanyDepartmentID
			INNER JOIN dbo.CONTACT C ON CD.TimesheetApproverID = C.CONTACT_ID
	/* Timesheet Approver concat string and xml, replaces expensive UDF */
	SELECT
		TimesheetID,
		STUFF(
			(
				SELECT ', ' + Contact
				FROM #TimesheetApprovers
				WHERE (TimesheetID = lv2.TimesheetID)
				FOR XML PATH ('')
			),1,2,'') AS TimesheetApproverNames,
		(
			SELECT
				Contact_ID [@ContactID],
				Contact [@ContactName]
			FROM	#TimesheetApprovers
			WHERE	TimesheetID= lv2.TimesheetID
			FOR XML PATH('TimesheetApprover'), ROOT('TimesheetApprovers')
		) AS TimesheetApprovers
	INTO #normalized_ta
	FROM #TimesheetApprovers lv2
	GROUP BY
		TimesheetID

	/* previously this was done in the final select as separate outer join for each measure */
	SELECT
		t.TimesheetID,
		SUM(CASE WHEN trc.isRegularHours = 1 THEN Hours ELSE 0 END) AS Reg_Hours,
		SUM(CASE WHEN trc.isOvertimeHours = 1 THEN Hours ELSE 0 END) AS Ovt_Hours,
		SUM(CASE WHEN trc.isHolidayHours = 1 THEN Hours ELSE 0 END) AS Hol_Hours,
		SUM(CASE WHEN trc.IsDoubleTimeHours = 1 THEN Hours ELSE 0 END) AS DT_Hours,
		SUM(CASE WHEN
			-- exclude all cases above
			trc.isRegularHours = 0 AND trc.isOvertimeHours = 0 AND trc.isHolidayHours = 0
			-- Expenses are not hours :)
			AND trc.isExpense = 0 THEN Hours ELSE 0 END) AS Other_Hours
	INTO #Timesheet_Hours
	FROM #Timesheet t
		INNER JOIN dbo.TimesheetLineEntry td ON t.TimesheetID = td.TimesheetID
		INNER JOIN dbo.RateDetail mr ON td.RateID = mr.DetailID
		INNER JOIN dbo.TimesheetTRC trc ON trc.TRCID = mr.TRCID
	GROUP BY
		t.TimesheetID

	/* replaces former usage of fnConcatenateMatchTimesheetValues */	
	SELECT
		M.MATCH_ID,
		1 AS FieldID,
		CONVERT(VARCHAR(MAX),
			CASE
				WHEN C.SpecialTimesheetLine1 = 'PO' THEN
					STUFF(
						(
							SELECT ', ' + cpn.Name
							FROM dbo.POSITION p
								INNER JOIN dbo.Match m2 ON p.POSITION_ID = m2.POSITION_ID
								INNER JOIN dbo.CompanyPONumbers cpn ON cpn.CompanyID = p.COMPANY_ID
								INNER JOIN dbo.MatchPONumbers mpn ON mpn.MatchID = m2.MATCH_ID AND mpn.PONumberID = cpn.RecordID
							WHERE   m2.MATCH_ID = m.MATCH_ID
									AND cpn.IsEnabled = 1
							FOR XML PATH ('')
						), 1, 2, '')
				ELSE M.AccountinglFieldValue1
			END) AS MatchTimesheetValue
	INTO #MatchTimeSheetValues
	FROM dbo.COMPANY C
		INNER JOIN dbo.POSITION P ON C.COMPANY_ID = P.COMPANY_ID
		INNER JOIN dbo.Match M ON P.POSITION_ID = M.POSITION_ID
	WHERE EXISTS (SELECT * FROM #Timesheet t WHERE M.MATCH_ID = t.MATCH_ID)
	UNION ALL
	SELECT
			M.MATCH_ID,
			2 AS FieldID,
			CONVERT(VARCHAR(MAX),
				CASE
					WHEN C.SpecialTimesheetLine2 = 'PO' THEN
						STUFF(
							(
								SELECT ', ' + cpn.Name
								FROM dbo.POSITION p
									INNER JOIN dbo.Match m2 ON p.POSITION_ID = m2.POSITION_ID
									INNER JOIN dbo.CompanyPONumbers cpn ON cpn.CompanyID = p.COMPANY_ID
									INNER JOIN dbo.MatchPONumbers mpn ON mpn.MatchID = m2.MATCH_ID AND mpn.PONumberID = cpn.RecordID
								WHERE
									m2.MATCH_ID = m.MATCH_ID 
									AND cpn.IsEnabled = 1  
								FOR XML PATH ('')),1,2,'') 
					ELSE M.AccountinglFieldValue2
				END) AS MatchTimesheetValue
		FROM	  dbo.COMPANY C
			  INNER JOIN dbo.POSITION P ON C.COMPANY_ID = P.COMPANY_ID
			  INNER JOIN dbo.Match M ON P.POSITION_ID = M.POSITION_ID
		WHERE  EXISTS (SELECT * FROM #Timesheet t WHERE M.MATCH_ID = t.MATCH_ID)	
		UNION ALL
		SELECT
			M.MATCH_ID,
			3 AS FieldID,
			CONVERT(VARCHAR(MAX),
				CASE
					WHEN C.SpecialTimesheetLine3 = 'PO' THEN
						STUFF(
							(
								SELECT ', ' + cpn.Name
								FROM dbo.POSITION p
									INNER JOIN dbo.Match m2 ON p.POSITION_ID = m2.POSITION_ID
									INNER JOIN dbo.CompanyPONumbers cpn ON cpn.CompanyID = p.COMPANY_ID
									INNER JOIN dbo.MatchPONumbers mpn ON mpn.MatchID = m2.MATCH_ID AND mpn.PONumberID = cpn.RecordID
								WHERE   m2.MATCH_ID = m.MATCH_ID
										AND cpn.IsEnabled = 1
								FOR XML PATH ('')
							), 1, 2, '')
				ELSE M.AccountinglFieldValue3 END) AS MatchTimesheetValue
		FROM dbo.COMPANY C
			INNER JOIN dbo.POSITION P ON C.COMPANY_ID = P.COMPANY_ID
			INNER JOIN dbo.Match M ON P.POSITION_ID = M.POSITION_ID
		WHERE EXISTS (SELECT * FROM #Timesheet t WHERE M.MATCH_ID = t.MATCH_ID)
		UNION ALL
	 	SELECT
			M.MATCH_ID,
			4 AS FieldID,
			CONVERT(VARCHAR(MAX), 
				CASE
					WHEN C.SpecialTimesheetLine4 = 'PO' THEN 
						STUFF(
							(
								SELECT ', ' + cpn.Name
								FROM dbo.POSITION p
									INNER JOIN dbo.Match m2 ON p.POSITION_ID = m2.POSITION_ID
									INNER JOIN dbo.CompanyPONumbers cpn ON cpn.CompanyID = p.COMPANY_ID
									INNER JOIN dbo.MatchPONumbers mpn ON mpn.MatchID = m2.MATCH_ID AND mpn.PONumberID = cpn.RecordID
								WHERE
									m2.MATCH_ID = m.MATCH_ID 
									AND cpn.IsEnabled = 1  
								FOR XML PATH ('')
							), 1, 2, '')
					ELSE M.AccountinglFieldValue4
				END) AS MatchTimesheetValue
		FROM dbo.COMPANY C
			INNER JOIN dbo.POSITION P ON C.COMPANY_ID = P.COMPANY_ID
			INNER JOIN dbo.Match M ON P.POSITION_ID = M.POSITION_ID
		WHERE EXISTS (SELECT * FROM #Timesheet t WHERE M.MATCH_ID = t.MATCH_ID)	
	       
	
		SELECT
			t.TimesheetID,
			t.StartDate AS StartDate,
			t.EndDate AS EndDate,
			t.PayPeriodEndDate,
			t.IsExported,
			t.TaxEstimated,
			t.IsAdjustment,
			t.Admin,
			t.WorkComp,
			t.Billed,
			t.MATCH_ID,
			t.START_DATE AS MatchStartDate,
			t.CNTR_END_DATE AS MatchEndDate,
			t.NotesToBackOffice,
			t.IsPendingAdditionalApproval,
	
			-- rework, these were by far the most expensive contributor to this proc
			isnull((SELECT Value FROM #normalized_linevalues WHERE TimesheetID = t.TimesheetID and FieldID = 1), (SELECT MatchTimesheetValue FROM #MatchTimesheetValues WHERE MATCH_ID = t.MATCH_ID AND FieldID = 1)) AS Value1 , -- rewritten eliminating udf's
			isnull((SELECT Value FROM #normalized_linevalues WHERE TimesheetID = t.TimesheetID and FieldID = 2), (SELECT MatchTimesheetValue FROM #MatchTimesheetValues WHERE MATCH_ID = t.MATCH_ID AND FieldID = 2)) AS Value2 , -- rewritten eliminating udf's
			isnull((SELECT Value FROM #normalized_linevalues WHERE TimesheetID = t.TimesheetID and FieldID = 3), (SELECT MatchTimesheetValue FROM #MatchTimesheetValues WHERE MATCH_ID = t.MATCH_ID AND FieldID = 3)) AS Value3 , -- rewritten eliminating udf's
			isnull((SELECT Value FROM #normalized_linevalues WHERE TimesheetID = t.TimesheetID and FieldID = 4), (SELECT MatchTimesheetValue FROM #MatchTimesheetValues WHERE MATCH_ID = t.MATCH_ID AND FieldID = 4)) AS Value4 , -- rewritten eliminating udf's
	
			cand.CANDIDATE_ID,
			cand.LAST_NAME + ', ' + cand.FIRST_NAME AS Candidate,
			t.POSITION_ID,
			t.POSITION_TITLE,
			t.PositionStreet1 AS JobLocation,
			BillingContact.CONTACT_ID AS BillToID,
			BillingContact.LAST_NAME + ', ' + BillingContact.FIRST_NAME AS BillToName,
			NULLIF(CONVERT(varchar(max), MAX(#normalized_ta.TimesheetApprovers)), '') AS TimesheetApprovers, -- reworked from UDF
			MAX(#normalized_ta.TimesheetApproverNames) AS TimesheetApproverNames, -- reworked from UDF
			t.COMPANY_ID,
			t.NAME AS COMP_NAME,
			ISNULL(Numbers.Hours ,0) AS HOURS,
			ISNULL(Numbers.Paid ,0) AS Paid,
			ISNULL(Numbers.ExpenseBilled ,0) AS ExpenseBilled,
			ISNULL(Numbers.ExpensePaid ,0) AS ExpensePaid,
			ISNULL(Numbers.ExpenseLoad ,0) AS ExpenseLoad,
			ISNULL(Numbers.Paid,0)  AS TotalPay,
			ISNULL(Numbers.Billed,0) AS TotalBill,
			ISNULL(Processing.ProcessedPercent, 0) AS TimesheetProcessedPercent,
			ISNULL(Processing.PayRecordCreatedPercent, 0) AS PayRecordCreatedPercent,
			ISNULL(Processing.PayRecordExportedPercent, 0) AS PayRecordExportedPercent,
			ISNULL(Processing.VoucherExportedPercent, 0) AS VoucherExportedPercent,
			ISNULL(Processing.InvoiceCreatedPercent, 0) AS InvoiceCreatedPercent,
			ISNULL(Processing.InvoiceExportedPercent, 0) AS InvoiceExportedPercent,
			ISNULL(Processing.ApprovedPercent, 0) AS ApprovedPercent,
			ISNULL(Processing.WipPostedPercent, 0) AS WipPostedPercent,
			Processing.PayRecordExportedDate,
			MAX(ISNULL(Reg_Hours, 0)) AS RegHours,
			MAX(ISNULL(Ovt_Hours, 0)) AS OvtHours,
			MAX(ISNULL(Hol_Hours, 0)) AS HolHours,
			MAX(ISNULL(DT_Hours, 0)) AS DTHours,
			MAX(ISNULL(Other_Hours, 0)) AS OtherHours,
			ISNULL(mr.BillRate, 0) AS BillRate,
			ISNULL(mr.PayRate, 0) AS PayRate,
			( ( MAX(ISNULL(Reg_Hours, 0)) + MAX(ISNULL(Other_Hours, 0)) ) * ISNULL(mr.PayRate, 0) ) + ( MAX(ISNULL(Ovt_Hours, 0)) * ISNULL(mr.OvtPayRate,0) ) AS TotalPayOld,
			MatchAccountingClass.Name AS AcctClass,
			TAX.NAME AS TaxClass,
			#dep.DEPTS AS Department,
			COALESCE(CandOwner.FIRST_NAME, '') + ' ' + COALESCE(CandOwner.LAST_NAME, '') AS CandRep,
			COALESCE(AccountExec.FIRST_NAME, '') + ' ' + COALESCE(AccountExec.LAST_NAME, '') AS AERep,
			OvertimeTypes.Description AS IsOvertimeEligible,
			workcomp.NAME AS WorkCompName,
	
			-- TODO:  rework
			CASE
				WHEN EXISTS (
					SELECT NULL
						FROM dbo.TimesheetLineEntry
						WHERE TimesheetID = t.TimesheetID ) THEN 1
				ELSE 0
			END AS HasDetails,
			V.Name AS VendorName,
			V.VendorID,
			cont.LAST_NAME + ', ' + cont.FIRST_NAME AS ContactName,
			cont.CONTACT_ID AS ContactID,
			approvedUser.Name AS ApprovedBy,
			t.ApprovedDate,
			submittedUser.Name AS SubmittedBy,
			cand.CheckPayableName,
			cand.HoldChecksInstructions,
			t.SubmittedDate,
			'' AS ClientData1,
			CASE
				WHEN dbo.RoundBanker(ISNULL(mr.PayRate, 0) * 1.5, 2) <> ISNULL(mr.OvtPayRate, 0) AND t.OvertimeType = 2
				THEN 'Overtime Rate <> 1.5*Regular Rate'
				ELSE ''
			END
			+
			CASE
				WHEN MAX(ISNULL(Ovt_Hours, 0)) > 0 AND OvertimeTypes.AllowOvertime = 0
				THEN 'Overtime Hours Not Allowed'
				ELSE ''
			END
			+
			CASE
				WHEN MAX(ISNULL(Reg_Hours, 0)) > ISNULL(OvertimeTypes.MaxRegularHours, 999)
				THEN 'Regular Hours Cannot Be > ' + CONVERT(VARCHAR, OvertimeTypes.MaxRegularHours)
				ELSE ''
			END
			+
			CASE
				WHEN ( isnull(DefaultCandidateAddress.CountryID, 0) = 220 AND DefaultCandidateAddress.StateID IS NULL )
					OR ( DefaultCandidateAddress.Street1 IS NULL OR DefaultCandidateAddress.Street1 < ' ' )
					OR ( DefaultCandidateAddress.PostalCode IS NULL OR DefaultCandidateAddress.PostalCode < ' ')
					OR ( DefaultCandidateAddress.City IS NULL OR DefaultCandidateAddress.City < ' ' )
				THEN 'Candidate Address is incomplete.<br>'
				ELSE ''
			END
			+
			CASE
				WHEN MatchAccountingClass.IsValidForContract = 0
				THEN 'Accounting Class not valid for Contract Placements.<br>'
				ELSE ''
			END
			+
			CASE
				WHEN TAX.RequiresW2 = 1
					AND ( cand.LivedInStateRecordID IS NULL
						OR (W4Info.Exemptions IS NULL AND StateTax.TaxFlag <> 'NA')
						OR cand.federalExemptions IS NULL
						OR cand.federalFilingStatus IS NULL
						OR cand.SSN IS NULL
						OR ( cand.BirthDate IS NULL OR cand.BirthDate = '' )
					)
				THEN 'Candidate Tax Info Missing/Incomplete.<br>'
				ELSE ''
			END
			+
			CASE
				WHEN ( isnull(DefaultBillingContactAddress.CountryID, 0) = 220 AND DefaultBillingContactAddress.StateID IS NULL )
					OR ( DefaultBillingContactAddress.Street1 IS NULL OR DefaultBillingContactAddress.Street1 < ' ' )
					OR ( DefaultBillingContactAddress.PostalCode IS NULL OR DefaultBillingContactAddress.PostalCode < ' ' )
					OR ( DefaultBillingContactAddress.City IS NULL OR DefaultBillingContactAddress.City < ' ' )
				THEN 'Bill To Contact''s Address is incomplete.<br>'
				ELSE ''
			END
			+
			CASE
				WHEN ((t.OvertimeType = 5) -- Overtime Type as Auto-Calculate (FB-11949)
					AND DATEDIFF(day, t.StartDate, t.EndDate) < 6) -- Split Timesheet
				THEN 'This is a split timesheet with overtime type as Auto-Calculate.<br>'
				ELSE ''
			END AS IsValid,
			TimesheetCreationOption,
			TimesheetRangeCalendarName,
			t.BatchName,
			t.AttachmentsWithInvoiceCount,
			t.AttachmentsForReviewCount,
			t.NotesCount,
			MIN(tls.Description) TimesheetStatus
		FROM #Timesheet t
			LEFT OUTER JOIN #normalized_ta
				ON t.TimesheetID = #normalized_ta.TimesheetID
			INNER JOIN dbo.CANDIDATE cand
				ON t.CANDIDATE_ID = cand.CANDIDATE_ID
			LEFT OUTER JOIN dbo.CNTR_BURDEN_WORK_COMP workcomp
				ON t.CNTR_WORK_COMP_ID = workcomp.WORK_COMP_ID
			LEFT OUTER JOIN dbo.OvertimeTypes
				ON t.OvertimeType = OvertimeTypes.ID
			LEFT OUTER JOIN dbo.CONTACT BillingContact
				ON t.CONTACT_ID_BILLING = BillingContact.CONTACT_ID
			LEFT OUTER JOIN dbo.CONTACT cont
				ON cont.CONTACT_ID = t.CONTACT_ID
			LEFT OUTER JOIN dbo.MatchAccountingClass
				ON t.AccountingClassID = MatchAccountingClass.ClassID
			LEFT OUTER JOIN #dep
				ON t.MATCH_ID = #dep.MATCH_ID
			LEFT JOIN #TimesheetAmount Numbers
				ON T.TimesheetID = Numbers.TimesheetID
			LEFT JOIN dbo.TimesheetLineStatus tls 
				ON ISNULL(Numbers.TimesheetStatusID,1) = tls.TimesheetLineStatusID
			LEFT JOIN #TimesheetProcessing Processing
				ON T.TimesheetID = Processing.TimesheetID
			LEFT OUTER JOIN #Timesheet_Hours
				ON t.TimesheetID = #Timesheet_Hours.TimesheetID -- reworked from redundant queries
			LEFT OUTER JOIN dbo.CNTR_BURDEN_TAX AS TAX
				ON t.CNTR_TAX_ID = TAX.TAX_ID
			LEFT OUTER JOIN dbo.Vendors V
				ON V.EntityID = cand.OwnerEntityID
				AND V.IsEnabled = 1
			LEFT OUTER JOIN dbo.HR_REPRESENTATIVE CandOwner
				ON CandOwner.UserID = t.CurrentCandidateOwnerID
			LEFT OUTER JOIN dbo.HR_REPRESENTATIVE AccountExec
				ON AccountExec.UserID = t.AccountExecID
			LEFT OUTER JOIN dbo.ObjectsWithUserID approvedUser
				ON approvedUser.UserID = t.ApprovedBy
			LEFT OUTER JOIN dbo.ObjectsWithUserID submittedUser
				ON submittedUser.UserID = t.SubmittedBy
			-- TODO:  simplify the complexity of dbo.vwMatchRates
			LEFT OUTER JOIN dbo.vwMatchRates mr
				ON mr.MatchID = t.Match_ID
			LEFT OUTER JOIN dbo.Addresses DefaultCandidateAddress
				ON cand.DefaultAddressID = DefaultCandidateAddress.AddressID
			LEFT OUTER JOIN dbo.States CandidateState
				ON DefaultCandidateAddress.StateID = CandidateState.StateID
			LEFT OUTER JOIN dbo.Addresses DefaultBillingContactAddress
				ON BillingContact.DefaultAddressID = DefaultBillingContactAddress.AddressID
			LEFT OUTER JOIN dbo.CandidateStateW4Info W4Info
				ON W4Info.RecordID = cand.LivedInStateRecordID
			LEFT OUTER JOIN dbo.TaxFilings StateTax
				ON W4Info.TaxFilingID = StateTax.TaxFilingID
		WHERE
			t.PENDING_STATUS >= 120
		GROUP BY 
			t.TimesheetID,
			t.StartDate,
			t.EndDate,
			t.PayPeriodEndDate,
			t.TaxEstimated,
			t.Admin,
			t.WorkComp,
			t.Billed,
			t.IsExported,
			t.IsAdjustment,
			t.MATCH_ID,
			t.START_DATE,
			t.CNTR_END_DATE,
			t.NotesToBackOffice,
			t.AccountinglFieldValue1,
			t.AccountinglFieldValue2,
			t.AccountinglFieldValue3,
			t.AccountinglFieldValue4,
			t.OvertimeType,
			t.IsPendingAdditionalApproval,
			OvertimeTypes.AllowOvertime,
			cand.CANDIDATE_ID,
			cand.LAST_NAME + ', ' + cand.FIRST_NAME,
			cand.LAST_NAME,
			DefaultCandidateAddress.StateID,
			DefaultCandidateAddress.Street1,
			DefaultCandidateAddress.PostalCode,
			DefaultCandidateAddress.City,
			DefaultCandidateAddress.CountryID,
			CandidateState.Name,
			W4Info.Exemptions,
			cand.federalFilingStatus,
			W4Info.TaxFilingID,
	  	StateTax.TaxFlag,
			cand.federalExemptions,
			cand.CheckPayableName,
			cand.HoldChecksInstructions,
			cand.SSN,
			cand.LivedInStateRecordID,
			t.POSITION_ID,
			t.POSITION_TITLE,
			t.PositionStreet1,
			BillingContact.CONTACT_ID,
			BillingContact.LAST_NAME + ', ' + BillingContact.FIRST_NAME,
			DefaultBillingContactAddress.City,
			DefaultBillingContactAddress.StateID,
			DefaultBillingContactAddress.Street1,
			DefaultBillingContactAddress.PostalCode,
			DefaultBillingContactAddress.CountryID,
			t.COMPANY_ID,
			t.NAME,
			Numbers.TimesheetID,
			Numbers.Hours,
			Numbers.Paid,
			Numbers.Billed,
			Numbers.ExpenseBilled,
			Numbers.ExpensePaid,
			Numbers.ExpenseLoad,
			Processing.ProcessedPercent,
			Processing.PayRecordCreatedPercent,
			Processing.PayRecordExportedPercent,
			Processing.VoucherExportedPercent,
			Processing.InvoiceCreatedPercent,
			Processing.InvoiceExportedPercent,
			Processing.ApprovedPercent,
			Processing.PayRecordExportedDate,
			Processing.WipPostedPercent,
			mr.PayRate,
			mr.BillRate,
			mr.OvtPayRate,
			MatchAccountingClass.IsvalidForContract,
			MatchAccountingClass.Name,
			TAX.NAME,
			TAX.RequiresW2,
			#dep.DEPTS,
			OvertimeTypes.Description,
			workcomp.NAME,
			V.Name,
			V.VendorID,
			cont.LAST_NAME + ', ' + cont.FIRST_NAME,
			cont.CONTACT_ID,
			CandOwner.FIRST_NAME ,
			CandOwner.LAST_NAME ,
			AccountExec.FIRST_NAME ,
			AccountExec.LAST_NAME ,
			approvedUser.Name ,
			t.ApprovedDate ,
			submittedUser.Name ,
			t.SubmittedDate ,
			cand.taxState ,
			cand.BirthDate ,
			OvertimeTypes.MaxRegularHours,
			TimesheetCreationOption,
			TimesheetRangeCalendarName,
			BatchName,
			t.AttachmentsWithInvoiceCount,
			t.AttachmentsForReviewCount,
			t.NotesCount
		ORDER BY 
			cand.LAST_NAME ,
			BillToName
	END TRY
	BEGIN CATCH
		SELECT @ErrorMsg = OBJECT_NAME ( @@PROCID ) + ': ERROR in procedure "'+ ERROR_PROCEDURE() + '", Line ' + CAST( ERROR_LINE() AS VARCHAR(16) ) + ': ' + ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY();

		IF XACT_STATE() <> 0 ROLLBACK TRANSACTION

		RAISERROR(@ErrorMsg, @ErrorSeverity, 1)
		RETURN ( -1 )
	END CATCH

	-- these aren't really necessary
	IF OBJECT_ID('tempdb..#dep') is not null
		DROP TABLE #dep

	IF OBJECT_ID('tempdb..#Timesheet') is not null
		DROP TABLE #Timesheet
	
	IF OBJECT_ID('tempdb..#TimesheetAmount') is not null
		DROP TABLE #TimesheetAmount
	
	IF OBJECT_ID('tempdb..#TimesheetProcessing') is not null
		DROP TABLE #TimesheetProcessing

 GO

﻿IF EXISTS(SELECT * FROM	dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[spGetDuplicateContacts]'))
EXECUTE('DROP PROCEDURE [dbo].[spGetDuplicateContacts]')

PRINT 'Creating/Updating PROCEDURE [dbo].[spGetDuplicateContacts]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* This is used by Contact New page. */
CREATE PROCEDURE [dbo].[spGetDuplicateContacts]
    @lname VARCHAR(25) = NULL ,
    @fname VARCHAR(25) = NULL ,
    @phone1 VARCHAR(25) = NULL ,
    @phone2 VARCHAR(25) = NULL ,
    @phone3 VARCHAR(25) = NULL ,
    @email VARCHAR(50) = NULL ,
    @address1 VARCHAR(150) = NULL ,
    @zip VARCHAR(5) = NULL ,
    @entity UNIQUEIDENTIFIER = NULL
AS 
    IF ( @phone1 = '' ) 
        SET @phone1 = NULL;
    IF ( @phone2 = '' ) 
        SET @phone2 = NULL;
    IF ( @phone3 = '' ) 
        SET @phone3 = NULL;
    IF ( @email = '' ) 
        SET @email = NULL;

    SELECT  REPLACE(ISNULL(c.FIRST_NAME, ''), ISNULL(@fname,''),
                    '<span class=''highlight''>' + ISNULL(@fname,'') + '</span>') AS FirstName ,
            REPLACE(ISNULL(c.LAST_NAME, ''), ISNULL(@lname,''),
                    '<span class=''highlight''>' + ISNULL(@lname,'') + '</span>') AS LastName ,
            REPLACE(DefaultAddress.PostalCode, ISNULL(@zip, ''),
                    '<span class=''highlight''>' + ISNULL(@zip, '')
                    + '</span>') AS PostalCode ,
            REPLACE(DefaultAddress.Street1, @address1,
                    '<span class=''highlight''>' + @address1 + '</span>') AS Address1 ,
            REPLACE(REPLACE(REPLACE((SELECT Communication FROM dbo.fnConcatenateCommunicationsByCategory(c.CONTACT_ID, 2, 100, 1)),
                                    ISNULL(@phone1, ''),
                                    '<span class=''highlight''>'
                                    + ISNULL(@phone1, '') + '</span>'),
                            ISNULL(@phone2, ''),
                            '<span class=''highlight''>' + ISNULL(@phone2, '')
                            + '</span>'), ISNULL(@phone3, ''),
                    '<span class=''highlight''>' + ISNULL(@phone3, '')
                    + '</span>') AS PhoneNumbers ,
            c.CONTACT_ID AS ContactID ,
            c.COMPANY_ID AS CompanyID ,
            C.TITLE AS CurrentTitle ,
            co.NAME AS CompanyName ,
            REPLACE((SELECT Communication FROM dbo.fnConcatenateCommunicationsByCategory(c.CONTACT_ID, 2, 200, 1)),
                    ISNULL(@email, ''),
                    '<span class=''highlight''>' + ISNULL(@email, '')
                    + '</span>') AS Emails ,
            LastNote.LastNoteID ,
            LastNote.LastNoteBody ,
            LastNote.LastNoteCreatedBy ,
            LastNote.LastNoteDate ,
            LastNote.LastNoteActionName ,
            LastNote.LastNoteByName ,
            REPLACE(ISNULL(c.FIRST_NAME, ''), ISNULL(@fname,''),
                    '<span class=''highlight''>' + ISNULL(@fname,'') + '</span>')
            + SPACE(1) + REPLACE(ISNULL(c.LAST_NAME, ''), ISNULL(@lname,''),
                                 '<span class=''highlight''>' + ISNULL(@lname,'')
                                 + '</span>') AS FullName ,
            cs.Color AS StatusColor ,
            cs.IconID ,
            cs.Name AS StatusName
    FROM    ( SELECT    ReferenceID AS CONTACT_ID
              FROM      Communications
              WHERE     AboutTypeID = 2
                        AND Value IN ( @phone1, @phone2, @phone3, @email )
              UNION
              SELECT    CONTACT_ID
              FROM      CONTACT
              LEFT OUTER JOIN Addresses DefaultAddress ON CONTACT.DefaultAddressID = DefaultAddress.AddressID
              WHERE     LAST_NAME = @lname
                        AND FIRST_NAME = @fname
                        AND ( @zip = LEFT(DefaultAddress.PostalCode, 5)
                              OR DefaultAddress.PostalCode IS NULL
                              OR @zip IS NULL
                            )
              GROUP BY  CONTACT_ID
            ) a
            JOIN CONTACT c ON a.CONTACT_ID = c.CONTACT_ID
            JOIN COMPANY co ON c.COMPANY_ID = co.COMPANY_ID
                               AND ( @entity IS NULL
                                     OR co.OwnerEntityID = @entity
                                   )
            OUTER APPLY ( SELECT TOP 1
                                    NI.NoteID LastNoteID ,
                                    NI.Body LastNoteBody ,
                                    NI.CreatedBy LastNoteCreatedBy ,
                                    NI.CreatedOn LastNoteDate ,
                                    COALESCE(HRI.FIRST_NAME, '') + ' ' + COALESCE(HRI.LAST_NAME, '') LastNoteByName ,
                                    NA.Name AS LastNoteActionName
                          FROM      dbo.NewNotes NI ( NOLOCK )
                                    JOIN dbo.HR_REPRESENTATIVE HRI ( NOLOCK ) ON NI.CreatedBy = HRI.UserID
                                    JOIN dbo.NoteActions NA ( NOLOCK ) ON NA.ActionID = NI.ActionID
                          WHERE     NI.NoteID = C.LastNoteID
                                    AND NA.Permission IS NULL
                        ) LastNote
            LEFT OUTER JOIN dbo.ContactStatus cs ON c.StatusID = cs.StatusID
            LEFT OUTER JOIN dbo.Addresses DefaultAddress (NOLOCK) ON C.DefaultAddressID = DefaultAddress.AddressID
    WHERE   c.IsMergedTo IS NULL
 
		
	

GO

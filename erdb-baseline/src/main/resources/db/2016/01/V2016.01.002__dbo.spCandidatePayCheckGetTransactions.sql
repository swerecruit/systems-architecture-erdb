﻿IF EXISTS(SELECT * FROM	dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[spCandidatePayCheckGetTransactions]'))
EXECUTE('DROP PROCEDURE [dbo].[spCandidatePayCheckGetTransactions]')

PRINT 'Creating/Updating PROCEDURE [dbo].[spCandidatePayCheckGetTransactions]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Desplenter
-- Create date: 2013.02.15, modifed on 2015.10.16 with Outsource version
-- Description:	Retrieve a list of transactions in GP for the given paycheck
-- =============================================
CREATE PROCEDURE [dbo].[spCandidatePayCheckGetTransactions]
	@CandidateID int,
	@CheckNumber varchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @EmployID VARCHAR(50);
	SET @EmployID = (SELECT ExternalPayrollID FROM dbo.CANDIDATE WHERE candidate_id = @CandidateID);

	IF (EXISTS (SELECT 1 FROM sys.synonyms WHERE name = 'CAL_Check_General_CALGPCKS') OR EXISTS (SELECT 1 FROM sys.views WHERE name = 'CAL_Check_General_CALGPCKS')) BEGIN

		SELECT 
				ch.COMPTRTP as PayrollTypeID,
					case ch.COMPTRTP
						when 1 then 'Pay Codes'
						when 2 then 'Deductions'
						when 3 then 'Benefits'
						when 4 then 'State Taxes'
						when 5 then 'Local Taxes'
					end as PayrollTypeName,
					ch.UPRTRXCD as PayrollCode,
					case ch.COMPTRTP
						when 1 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_Pay_Type WHERE PAYRCORD = ch.UPRTRXCD)
						when 2 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_Deduction WHERE DEDUCTON = ch.UPRTRXCD)
						when 3 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_Benefit WHERE BENEFIT = ch.UPRTRXCD)
						when 5 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_LOCAL_TAX WHERE LOCALTAX = ch.UPRTRXCD)
						else (SELECT TOP 1 Name FROM States WHERE StateCode = ch.UPRTRXCD)
					end as PayrollCodeName,
			[PayRate] =  case when trc.isExpense=1 then null else ch.HRLYPYRT end, --case WHEN ch.HRLYPYRT > 0 THEN ch.HRLYPYRT ELSE SUM(PAYRTAMT) END,
			[PayUnits] = case when trc.isExpense=1 then null else convert(numeric,SUM(ch.TRXHRUNT))/100 end,
			[Amount] = case WHEN ch.HRLYPYRT > 0 THEN ch.HRLYPYRT * (convert(numeric,SUM(ch.TRXHRUNT))/100) ELSE SUM(ch.PAYRTAMT) END,
					CONVERT(VARCHAR, MAX(t.StartDate), 101) + ' - ' + CONVERT(VARCHAR, MAX(t.EndDate), 101) as WeekWorked,
			cm.NAME as ClientName,
			CAST(CASE WHEN (MAX(pc.PAYTYPE) = 5 or MAX(pc.PAYTYPE) = 12) AND MAX(pc.TAXABLE) = 0 THEN 0 ELSE 1 END AS bit) AS IsTaxable  --,
			--ch.EMPLOYID,
			--ch.CHEKNMBR,
			--ch.jobtitle,
			--[Company] = cm.NAME,
			--[PayPeriod] = convert(nvarchar,t.StartDate,101) +'-'+ convert(nvarchar,t.EndDate,101),
			--[Match] = t.MatchID,
			--[Wage Code] = trc.payrollEDCode, --case when trc.isExpense=1 then 'Expense' else trc.PayrollEDCode end,
			--[Hours] = case when trc.isExpense=1 then null else convert(numeric,SUM(ch.TRXHRUNT))/100 end,
			--[YTD Hours]=case when trc.isExpense=1 then null else x.ytdhours end,
			--[Rate] =  case when trc.isExpense=1 then null else ch.HRLYPYRT end, --case WHEN ch.HRLYPYRT > 0 THEN ch.HRLYPYRT ELSE SUM(PAYRTAMT) END,
			--[Gross Wages] = case WHEN ch.HRLYPYRT > 0 THEN ch.HRLYPYRT * (convert(numeric,SUM(ch.TRXHRUNT))/100) ELSE SUM(PAYRTAMT) END,
			--[YTD Gross Wages]=case when trc.isExpense=0 then x.YTDGrossWages when trc.IsExpense=1 then x.MiscExpense end
		FROM GreatPlains.CAL_Check_General_CALGPCKS ch
			INNER JOIN GreatPlains.UPR_SETP_Pay_Type pc ON pc.PAYRCORD = ch.UPRTRXCD
			INNER JOIN GreatPlains.UPR_Check_HIST upch on ch.EMPLOYID=upch.EMPLOYID and ch.CHEKNMBR=upch.CHEKNMBR and ch.CHEKDATE=upch.CHEKDATE and ch.PYADNMBR=upch.PYADNMBR
			INNER JOIN dbo.Timesheet t on replace(convert(nvarchar(25),ch.TXTFIELD),'ERM','') = t.TimesheetID
			INNER JOIN	dbo.Match m ON t.MatchID = m.MATCH_ID
			INNER JOIN dbo.CANDIDATE c ON m.CANDIDATE_ID = c.CANDIDATE_ID
			INNER JOIN	dbo.POSITION ON m.POSITION_ID = dbo.POSITION.POSITION_ID
			INNER JOIN dbo.COMPANY cm ON dbo.POSITION.COMPANY_ID = cm.COMPANY_ID
			CROSS APPLY (SELECT TOP 1 PayrollEDCode,isExpense from dbo.TimesheetTRC where PayrollEDCode=ch.UPRTRXCD) trc  
			CROSS APPLY (select convert(numeric,SUM(ch2.TRXHRUNT))/100 as ytdhours,convert(numeric,SUM(ch2.TRXHRUNT * ch2.HRLYPYRT))/100 as 
			YTDGrossWages,SUM(PAYRTAMT) as MiscExpense from GreatPlains.CAL_Check_General_CALGPCKS ch2 where ch2.EMPLOYID=ch.EMPLOYID and ch2.UPRTRXCD=ch.UPRTRXCD
			and DATEPART(YYYY,ch2.CHEKDATE)=DATEPART(YYYY,GETDATE())) x
	
		WHERE ch.CHEKNMBR = @CheckNumber and 
			 (ch.TRXHRUNT<>0 or (ch.TRXHRUNT=0 and ch.PAYRTAMT<>0))
		AND ch.EMPLOYID = @EmployID
		GROUP BY convert(nvarchar,CH.TXTFIELD),ch.EMPLOYID,ch.CHEKNMBR,CH.JOBTITLE,cm.NAME,t.StartDate,t.EndDate,
		t.MatchID,trc.PayrollEDCode,ch.HRLYPYRT,x.ytdhours,x.YTDGrossWages,trc.isExpense,x.MiscExpense, ch.COMPTRTP, ch.UPRTRXCD

		union all

		SELECT	T.PYRLRTYP as PayrollTypeID,
			case T.PYRLRTYP
				when 1 then 'Pay Codes'
				when 2 then 'Deductions'
				when 3 then 'Benefits'
				when 4 then 'State Taxes'
				when 5 then 'Local Taxes'
			end as PayrollTypeName,
			T.PAYROLCD as PayrollCode,
			case T.PYRLRTYP
				when 1 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_Pay_Type WHERE PAYRCORD = T.PAYROLCD)
				when 2 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_Deduction WHERE DEDUCTON = T.PAYROLCD)
				when 3 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_Benefit WHERE BENEFIT = T.PAYROLCD)
				when 5 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_LOCAL_TAX WHERE LOCALTAX = T.PAYROLCD)
				else (SELECT TOP 1 Name FROM States WHERE StateCode = T.PAYROLCD)
			end as PayrollCodeName,
			T.PAYRATE as PayRate,
			SUM(T.UNTSTOPY) as PayUnits,
			SUM(T.UPRTRXAM) as Amount,
			CONVERT(VARCHAR, MAX(ts.StartDate), 101) + ' - ' + CONVERT(VARCHAR, MAX(ts.EndDate), 101) as WeekWorked,
			MAX(C.Name) as ClientName,
			NULL AS IsTaxable --does not apply
		FROM	GreatPlains.UPR_Transaction_HIST T
		CROSS APPLY (SELECT TOP 1 TXTFIELD FROM GreatPlains.CAL_Check_General_CALGPCKS WHERE T.EMPLOYID = EMPLOYID AND T.CHEKNMBR = CHEKNMBR AND T.PYRLRTYP<>1) CH
		LEFT JOIN dbo.Timesheet ts on CONVERT(INT, SUBSTRING(ch.TXTFIELD, LEN(CONVERT(VARCHAR(16), ch.TXTFIELD)) - 6, 7)) = ts.TimesheetID
		LEFT JOIN Match M on ts.MatchID = M.MATCH_ID
		LEFT JOIN POSITION P on M.POSITION_ID = P.POSITION_ID
		LEFT JOIN COMPANY C ON P.COMPANY_ID = C.COMPANY_ID
		WHERE	T.EMPLOYID = @EmployID
		AND		T.CHEKNMBR = @CheckNumber	
		GROUP BY T.PYRLRTYP, T.PAYROLCD, T.PAYRATE
		Order by PayrollTypeID, ClientName

	END ELSE BEGIN

		SELECT	T.PYRLRTYP as PayrollTypeID,
			case T.PYRLRTYP
				when 1 then 'Pay Codes'
				when 2 then 'Deductions'
				when 3 then 'Benefits'
				when 4 then 'State Taxes'
				when 5 then 'Local Taxes'
			end as PayrollTypeName,
			T.PAYROLCD as PayrollCode,
			case T.PYRLRTYP
				when 1 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_Pay_Type WHERE PAYRCORD = T.PAYROLCD)
				when 2 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_Deduction WHERE DEDUCTON = T.PAYROLCD)
				when 3 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_Benefit WHERE BENEFIT = T.PAYROLCD)
				when 5 then (SELECT TOP 1 DSCRIPTN FROM GreatPlains.UPR_SETP_LOCAL_TAX WHERE LOCALTAX = T.PAYROLCD)
				else (SELECT TOP 1 Name FROM States WHERE StateCode = T.PAYROLCD)
			end as PayrollCodeName,
			T.PAYRATE as PayRate,
			SUM(T.UNTSTOPY) as PayUnits,
			SUM(T.UPRTRXAM) as Amount
		FROM	GreatPlains.UPR_Transaction_HIST T
		WHERE	T.EMPLOYID = @EmployID
		AND		T.CHEKNMBR = @CheckNumber	
		GROUP BY T.PYRLRTYP, T.PAYROLCD, T.PAYRATE
		ORDER BY T.PYRLRTYP

	END
	
END

GO

 

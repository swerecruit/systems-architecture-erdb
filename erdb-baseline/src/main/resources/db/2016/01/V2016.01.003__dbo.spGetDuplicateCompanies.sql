﻿IF EXISTS(SELECT * FROM	dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[spGetDuplicateCompanies]'))
EXECUTE('DROP PROCEDURE [dbo].[spGetDuplicateCompanies]')

PRINT 'Creating/Updating PROCEDURE [dbo].[spGetDuplicateCompanies]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* This is used by Company New page */
CREATE PROCEDURE [dbo].[spGetDuplicateCompanies]
    @name VARCHAR(100) = NULL ,
    @legalname VARCHAR(100) = NULL ,
    @phone1 VARCHAR(25) = NULL ,
    @phone2 VARCHAR(25) = NULL ,
    @phone3 VARCHAR(25) = NULL ,
    @website VARCHAR(50) = NULL ,
    @address1 VARCHAR(150) = NULL ,
    @zip VARCHAR(5) = NULL ,
    @entity UNIQUEIDENTIFIER = NULL
AS 
    IF ( @phone1 = '' ) 
        SET @phone1 = NULL;
    IF ( @phone2 = '' ) 
        SET @phone2 = NULL;
    IF ( @phone3 = '' ) 
        SET @phone3 = NULL;
    IF ( @website = '' ) 
        SET @website = NULL;

    SELECT  REPLACE(c.NAME, @name,
                    '<span class=''highlight-old''>' + @name + '</span>') AS CompanyName ,
						REPLACE(c.LegalName, @legalname,
                    '<span class=''highlight-old''>' + @legalname + '</span>') AS LegalName ,
            REPLACE(DefaultAddress.PostalCode, ISNULL(@zip, ''),
                    '<span class=''highlight-old''>' + ISNULL(@zip, '')
                    + '</span>') AS PostalCode ,
            REPLACE(DefaultAddress.Street1, @address1,
                    '<span class=''highlight-old''>' + @address1 + '</span>') AS Address1 ,
            REPLACE(REPLACE(REPLACE((SELECT Communication FROM dbo.fnConcatenateCommunicationsByCategory(c.COMPANY_ID, 1, 100, 1)),
                                    ISNULL(@phone1, ''),
                                    '<span class=''highlight-old''>'
                                    + ISNULL(@phone1, '') + '</span>'),
                            ISNULL(@phone2, ''),
                            '<span class=''highlight-old''>' + ISNULL(@phone2, '')
                            + '</span>'), ISNULL(@phone3, ''),
                    '<span class=''highlight-old''>' + ISNULL(@phone3, '')
                    + '</span>') AS PhoneNumbers ,
            c.COMPANY_ID AS CompanyID ,
            REPLACE((SELECT Communication FROM dbo.fnConcatenateCommunicationsByCategory(c.COMPANY_ID, 1, 300, 1)),
                    ISNULL(@website, ''),
                    '<span class=''highlight-old''>' + ISNULL(@website, '')
                    + '</span>') AS Website ,
            LastNote.LastNoteID ,
            LastNote.LastNoteBody ,
            LastNote.LastNoteCreatedBy ,
            LastNote.LastNoteDate ,
            LastNote.LastNoteActionName ,
            LastNote.LastNoteByName ,
            cs.Color AS StatusColor ,
            cs.IconID ,
            cs.Name AS StatusName
    FROM    ( SELECT    ReferenceID AS COMPANY_ID
              FROM      Communications
              WHERE     AboutTypeID = 1
                        AND Value IN ( @phone1, @phone2, @phone3, @website )
              UNION
              SELECT    COMPANY_ID
              FROM      COMPANY
              WHERE     NAME = @name OR LegalName = @legalname
              GROUP BY  COMPANY_ID
            ) a
            JOIN COMPANY c ON a.COMPANY_ID = c.COMPANY_ID
                              AND ( @entity IS NULL
                                    OR c.OwnerEntityID = @entity
                                  )
            OUTER APPLY ( SELECT TOP 1
                                    NI.NoteID LastNoteID ,
                                    NI.Body LastNoteBody ,
                                    NI.CreatedBy LastNoteCreatedBy ,
                                    NI.CreatedOn LastNoteDate ,
                                    COALESCE(HRI.FIRST_NAME, '') + ' ' + COALESCE(HRI.LAST_NAME, '') LastNoteByName ,
                                    NA.Name AS LastNoteActionName
                          FROM      dbo.NewNotes NI ( NOLOCK )
                                    JOIN dbo.HR_REPRESENTATIVE HRI ( NOLOCK ) ON NI.CreatedBy = HRI.UserID
                                    JOIN dbo.NoteActions NA ( NOLOCK ) ON NA.ActionID = NI.ActionID
                          WHERE     NI.NoteID = C.LastNoteID
                                    AND NA.Permission IS NULL
                        ) LastNote
            LEFT OUTER JOIN CompanyStatus cs ON c.StatusID = cs.StatusID
            LEFT OUTER JOIN dbo.Addresses DefaultAddress (NOLOCK) ON C.DefaultAddressID = DefaultAddress.AddressID
    WHERE   c.IsMergedTo IS NULL
 
		
	

GO

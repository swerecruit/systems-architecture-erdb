﻿IF EXISTS(SELECT * FROM	dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[spViewCandidate]'))
EXECUTE('DROP PROCEDURE [dbo].[spViewCandidate]')

PRINT 'Creating/Updating PROCEDURE [dbo].[spViewCandidate]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spViewCandidate]
/********************************************************************************************************
Stored Procedure Name: spViewCandidate
Stored Procedure Description:
	Displays basic candidate information by company and user id
Stored Procedure Create Date: 3/9/2011
Stored Procedure Author: JEP

Sample Exec Statement:
EXEC dbo.spViewCandidate 
	@ReferenceID = 116801
	,@UserID = '212AA64F-D94C-4125-B357-CAE353829A1A'

Stored Procedure Modification History:
By		Date		Description
JEP		3/9/2011	-Created
					-Cost: 3.89175 on Demo01
JEP		3/10/2011	-Updated mappings, added profit and user
					-Cost: 3.93823 on Demo01
JEP		3/10/2011	-Added address data, ownerentityid, and timestamp
					-Cost: 3.9389
JEP		3/10/2011	-added counts
					-Cost: 6.20801
JEP		3/10/2011	-switch columns to IDs
					-Cost: 6.18487
JEP		3/11/2011	-added RequiresTaxes, OwnerCount
					-Cost: 6.19145
JEP		3/18/2011	-converting timestamp to varbinary
					-Cost: .488325
JEP		3/21/2011	-converting timestamp to numeric
JEP		3/28/2011	-added skill count
JEP		4/15/2011	-changed to degree to educationlevel
JEP		5/9/2011	-added LitigationHistoryCount
JEP		5/11/2011	-added general comments
SRC		5/12/2011	-Added SemanticProfile/SemanticXml
TD		5/13/2011	-Added Citizenship
BQP		5/17/2011	-Added LastLoginDate to support candidate lookups in the API
JEP		5/27/2011	-Added candidateapplication count
TD		6/1/2011	-Added IsHighlighted
RJ		8/19/2011	-Added NamePrefix and NameSuffix
RJ		8/30/2011	-Updated LastNoteByUser, Last Note and Notes Count
TD		10/28/2011	-Added MarketingCampaignCount
TD		11/22/2011	-Added RequirementCount
RK		12/20/2011	-Added OpportunityCount
TD		1/23/2012	-Added TimesheetCount
RJ		4/19/2012	-Added VeteranStatusID
TD		5/3/2012	-Added DoNotCall, DoNotText + return EmailOptOut as EmailOptOut instead of DoNotSolicit
RK		5/14/2012	-Fixed MarketingCampaignCount to join against candidate instead of contact
TD		8/16/2012	-Added PTOBank & PTOBankExpiration
TD		9/4/2012	-Added a whole bunch of name fields for the v3 manage page
TD		10/16/2012	-Fixed CONTRACTOR_TYPE/TAX_ID confusion
TD		10/30/2012	-Added Tax1099CompanyID
RJ		12/21/2012	-Added NonVMSCandidateVendorID
BS		2/13/2013	-Added FromSeedID, FromSeedName
TD		2/15/2013	-Added PayCheckCount
FS		2/21/2013	-Removed IsHighlighted flag
TD		3/21/2013	-Added DefaultAddress stuff
TD		4/25/2013	-Added CandidateReferenceCount
TD		5/7/2013	-Added ExternalPayrollID, OwnerEntityName
TD		5/10/2013	-Added TimesheasyVendorName
RJ		5/21/2013	-Added TerminateDate
RJ		7/31/2013 -Added 
AB    11/19/2013 -Added ClaimCount
AB    11/25/2013 -Added ACAEmployeeType
RO		9/25/2014 -Moved params into local vars to disable param sniffing issue
*********************************************************************************************************/
    (
      @ReferenceID INT ,
      @UserID UNIQUEIDENTIFIER,
			@LoggedInModeSharingOption INT = 0,
			@DimensionValueLevel INT = 1,		-- This is the level at which statuses can be linked to Department Dimension Values
      @LoggedInAsMode INT
    )
AS 
BEGIN

	SET NOCOUNT ON;

	DECLARE @AboutTypeID SMALLINT
	DECLARE @localReferenceID INT
	DECLARE @localUserID UNIQUEIDENTIFIER
	DECLARE @localLoggedInModeSharingOption INT

	SET @localReferenceID = @ReferenceID;
	SET @localUserID = @UserID;
	SET @localLoggedInModeSharingOption = @LoggedInModeSharingOption;

	SELECT  @AboutTypeID = AboutTypeID
	FROM    dbo.AboutTypes (NOLOCK)
	WHERE   AboutTypeName = 'Candidate'

	SELECT
		C.FIRST_NAME FirstName ,
		C.LAST_NAME LastName ,
		C.MiddleName MiddleName ,
		C.NickName NickName ,
		C.NamePrefix ,
		C.NameSuffix ,
		C.FOLDER_GROUP_ID DefaultFolderGroup, --?
		C.DefaultResumeID DefaultResumeID ,
		C.Gender Gender ,
		C.CURRENT_EMPLOYER CurrentEmployer ,
		C.CURRENT_TITLE Title ,
		C.EducationLevelID ,
		educationLevels.DisplayName EducationLevelName ,
		C.CellphoneProviderID CellProviderID ,
		cell.Description CellProviderName ,
		C.EmailOptOut EmailOptOut ,
		C.EEOCategoryID EEOCategoryID ,
		eeo.Name EEOCategoryName ,
		C.EthnicityID EthnicityID ,
		C.IsLatino IsLatino,
		eth.Name EthnicityName ,
		C.OwnerUserID OwnerUserID ,
		C.HR_REP_ID OwnerID ,
		hrOwner.FIRST_NAME OwnerFirstName ,
		hrOwner.LAST_NAME OwnerLastName ,
		C.OwnershipExpirationDate OwnershipExpiresOn ,
		C.IsOwnershipPermanent IsOwnershipPermanent ,
		C.LeadUserID ,
		C.HR_REP_ID_LEAD LeadID ,
		hrLead.FIRST_NAME LeadFirstName ,
		hrLead.LAST_NAME LeadLastName ,
		C.FULL_TIME IsLookingForPerm ,
		C.CONTRACT IsLookingForContract, --?
		C.CONTRACT_PERM IsLookingForContractToPerm, --?
		C.STATUS_ID StatusID ,
		candidateStatus.STATUS_DESC StatusName ,
		candidateStatus.IconID StatusIconID ,
		C.RATING RatingID ,
		candidateRating.Desc_Rating RatingName ,
		C.TAX_ID ContractorTypeID ,
		tax.NAME ContractorTypeName ,
		C.DATE_AVAILABLE AvailableDate ,
		C.HOURS_PER_WEEK_MIN MinHoursPerWeek ,
		C.HOURS_PER_WEEK_MAX MaxHoursPerWeek ,
		C.TRAVEL_PERCENTAGE MaxTravelPercentage, --?
		C.COMMUTE_MINUTES MaxCommute, --?
		C.ClearanceLevelID ClearanceLevelID ,
		clearanceLevels.Name ClearanceLevelName ,
		C.CURRENT_SALARY YearlyMin ,
		C.DESIRED_SALARY YearlyMax ,
		C.CURRENT_HOURLY_SALARY HourlyMin ,
		C.DESIRED_HOURLY_SALARY HourlyMax ,
		C.GeneralComments GeneralComments ,
		C.Citizenship Citizenship ,
		ISNULL((
			SELECT IsApproved
			FROM   dbo.aspnet_Membership (NOLOCK)
			WHERE  UserId = C.UserID
		), 0) IsApproved, --?
		C.UserID ,
		LastNote.LastNoteID ,
		LastNote.LastNoteBody ,
		LastNote.LastNoteCreatedBy ,
		LastNote.LastNoteCreatedOn ,
		LastNote.LastNoteCreatedByName ,
		LastNoteByUser.LastNoteIDByUser ,
		LastNoteByUser.LastNoteBodyByUser ,
		LastNoteByUser.LastNoteCreatedOnByUser ,
		TC.TotalHoursWorked ,
		TC.TotalProfitGenerated TotalProfit ,
		C.OwnerEntityID ,
		OwnerEntity.Name as OwnerEntityName ,
		OwnerEntity.EntityTypeID as OwnerEntityType,
		CONVERT(NUMERIC(20, 0), C.Timestamp + 0) Timestamp ,
		C.CANDIDATE_SUMMARY Summary ,
		C.IsLeadPermanent ,
		C.DateOwnershipRenewed ,
		C.AD_SOURCE AdSource ,
		C.AdsourceAdditionalInfo ,
		C.SemanticXml ,
		C.SemanticProfile ,
		C.VeteranStatusID ,
		vetStatus.Name VeteranStatusName ,
		C.DisabilityStatusID ,
		disabilityStatuses.Name DisabilityStatusName ,
		C.DoNotCall ,
		C.DoNotText ,
		C.PTOBank ,
		C.PTOBankExpiration ,
		Tax1099Company.COMPANY_ID Tax1099CompanyID,
		Tax1099Company.NAME Tax1099CompanyName,
		IsNull(Tax1099Company.NAME, C.CheckPayableName) as TimesheasyVendorName,
		C.NonVMSCandidateVendorID,
		AM.LastLoginDate ,
		C.ACAEmployeeType ,
		C.ExternalPayrollID ,
		C.TerminateDate ,
		C.TerminationReasonID ,
		terminationreason.Name as TerminationReasonName ,
		seed.SeedID FromSeedID,
		seed.HarvestDisplayName AS FromSeedName,
		marketo.MarketoLeadID AS MarketoLeadID , 
		C.DefaultAddressID AddressID ,
		DefaultAddress.CommunicationTypeID CommunicationTypeID,
		DefaultAddress.Name AddressName ,
		DefaultAddress.Street1 AddressLine1 ,
		DefaultAddress.Street2 AddressLine2 ,
		DefaultAddress.City City ,
		DefaultAddress.PostalCode PostalCode ,
		DefaultAddress.StateID StateID ,
		DefaultAddress.RegionID RegionID ,
		DefaultAddress.CountryID CountryID ,
		DefaultAddressState.Name StateName ,
		DefaultAddressRegion.Name RegionName ,
		DefaultAddressCountry.Name CountryName ,
		C.IsMergedTo ,
		ISNULL(MergedRecord.FIRST_NAME, '') + ' ' + ISNULL(MergedRecord.LAST_NAME, '') as MergedToName ,
		(
			SELECT COUNT(*)
			FROM dbo.AddressReferences AR ( NOLOCK )
			WHERE
				AR.AboutTypeID = @AboutTypeID
				AND AR.ReferenceID = C.CANDIDATE_ID
		) AddressCount ,
		(
			SELECT COUNT(*)
			FROM dbo.NoteReferences RI ( NOLOCK )
				JOIN dbo.NewNotes NI ( NOLOCK ) ON RI.NoteID = NI.NoteID
				JOIN dbo.NoteActions NA ( NOLOCK ) ON NA.ActionID = NI.ActionID
			WHERE RI.AboutTypeID = @AboutTypeID
				AND RI.ReferenceID = @localReferenceID
				AND NA.Permission IS NULL
		) NoteCount ,
		(
			SELECT COUNT(MI.MATCH_ID)
			FROM dbo.Match MI ( NOLOCK )
			WHERE MI.CANDIDATE_ID = C.CANDIDATE_ID
		) MatchCount,
		(
			SELECT COUNT(*)
			FROM dbo.CandidateCompanyDNS DNS ( NOLOCK )
			WHERE DNS.CANDIDATEID = C.CANDIDATE_ID
		) DoNotSendCount ,
		(
			SELECT COUNT(DISTINCT LI.ListID)
			FROM
				dbo.Lists LI ( NOLOCK )
				JOIN dbo.ListMembers LMI ( NOLOCK ) ON LI.ListID = LMI.ListID
				LEFT JOIN dbo.ListAccess LAI ( NOLOCK ) ON LI.ListID = LAI.ListID
			WHERE
				LI.AboutTypeID = @AboutTypeID
				AND LMI.ReferenceID = C.CANDIDATE_ID
				AND ( CreatedBy = @localUserID
					OR IsPublicRead = 1
					OR LAI.UserID = @localUserID
				)
		) ListCount ,
		(
			SELECT COUNT(*)
			FROM dbo.ObjectFolderGroups FI ( NOLOCK )
			WHERE FI.AboutTypeID = @AboutTypeID
				AND FI.ReferenceID = C.CANDIDATE_ID
		) FolderGroupCount ,
		(
			SELECT COUNT(*)
			FROM dbo.ObjectIndustries OI ( NOLOCK )
			WHERE OI.AboutTypeID = @AboutTypeID
				AND OI.ReferenceID = C.CANDIDATE_ID
		) IndustryCount ,
		(
			SELECT COUNT(*)
			FROM dbo.ObjectRelationships RI ( NOLOCK )
			WHERE
				( RI.AboutTypeIDFrom = @AboutTypeID AND RI.ReferenceIDFrom = C.CANDIDATE_ID )
				OR ( RI.AboutTypeIDTo = @AboutTypeID AND RI.ReferenceIDTo = C.CANDIDATE_ID )
		) RelationshipCount,
		(
			SELECT COUNT(*)
			FROM
				dbo.Attachments AI ( NOLOCK )
				JOIN dbo.AttachmentTypes ATI ( NOLOCK ) ON AI.TypeID = ATI.TypeID
			WHERE
				AI.DeletedOn IS NULL
				AND ParentAttachmentID IS NULL
				AND AI.AboutTypeID = @AboutTypeID
				AND AI.ReferenceID = C.CANDIDATE_ID
				AND ATI.CategoryID = 1
				AND ((AI.SharingOptionFlag & @localLoggedInModeSharingOption <> 0) OR AI.AddedBy = @localUserID)
		) AttachmentGeneralCount ,
		(
			SELECT COUNT(*)
			FROM dbo.Attachments AI ( NOLOCK )
				JOIN dbo.AttachmentTypes ATI ( NOLOCK ) ON AI.TypeID = ATI.TypeID
			WHERE
				AI.DeletedOn IS NULL
				AND ParentAttachmentID IS NULL
				AND AI.AboutTypeID = @AboutTypeID
				AND AI.ReferenceID = C.CANDIDATE_ID
				AND ATI.CategoryID = 10
		) AttachmentHRCount ,
		(
			SELECT
				CASE
					WHEN Requires1099 = 1 OR RequiresW2 = 1 THEN 1
					ELSE 0
				END RequiresTaxes
			FROM dbo.CNTR_BURDEN_TAX (NOLOCK)
			WHERE TAX_ID = C.TAX_ID
		) RequiresTaxes ,
		(
			SELECT
				COUNT(*)
			FROM dbo.AdditionalOwners (NOLOCK)
			WHERE
				ReferenceID = C.CANDIDATE_ID
				AND AboutTypeID = @AboutTypeID
		) AdditionalOwnersCount ,
		(
			SELECT
				COUNT(*)
			FROM
				dbo.CandidateSkills (NOLOCK)
			WHERE
				CandidateID = C.CANDIDATE_ID
		) SkillCount ,
		(
			SELECT
				COUNT(*)
			FROM dbo.ObjectRequirements REQ (NOLOCK)
			WHERE ReferenceID = C.CANDIDATE_ID and AboutTypeID = @AboutTypeID AND
					EXISTS(SELECT 1 
						   FROM  dbo.RequirementAccessContexts ctx 
						   WHERE ctx.RequirementID = REQ.RequirementID AND ctx.CanRead = 1 AND ctx.LoggedInAsMode = @LoggedInAsMode)
		) RequirementCount ,
		(
			SELECT COUNT(*)
			FROM dbo.LitigationHistory (NOLOCK)
			WHERE     CandidateID = C.CANDIDATE_ID
		) LitigationHistoryCount ,
		(
			SELECT COUNT(*)
			FROM dbo.CandidateApplications (NOLOCK)
			WHERE CandidateID = C.CANDIDATE_ID
		) CandidateApplicationCount ,
		(
			SELECT COUNT(*)
			FROM dbo.MarketingCampaignPeople (NOLOCK)
			WHERE AboutType = 6
				AND ReferenceID = C.CANDIDATE_ID
		) MarketingCampaignCount ,
		(
			SELECT COUNT(*)
			FROM dbo.Opportunities (NOLOCK)
			WHERE CandidateID = C.CANDIDATE_ID
		) OpportunityCount ,
		(
			SELECT COUNT(*)
			FROM dbo.Timesheet TI ( NOLOCK )
				INNER JOIN dbo.Match M ON TI.MatchID = M.MATCH_ID
			WHERE     M.CANDIDATE_ID = C.CANDIDATE_ID
		) TimesheetCount,
		(
			SELECT CheckCount
			FROM dbo.fnGetCandidatePayCheckCount(@localReferenceID)
		) PayCheckCount,
		(
			SELECT COUNT(*)
			FROM dbo.CandidateReferences (NOLOCK)
			WHERE CandidateID = C.CANDIDATE_ID
		) CandidateReferenceCount,
		(
			SELECT COUNT(*)
			FROM dbo.CandidateClaims (NOLOCK)
			WHERE CandidateID = C.CANDIDATE_ID
		) ClaimCount,
		(
			SELECT COUNT(*)
			FROM
				dbo.AccrualSchedules (NOLOCK)
			WHERE IsActive = 1
		) AccrualScheduleCount,
		(
			SELECT COUNT(*)
			FROM
				dbo.CandidateAccrualSchedules (NOLOCK)
			WHERE CandidateID = C.CANDIDATE_ID
		) CandidateAccrualScheduleCount,
		(
			SELECT COUNT(*)
			FROM dbo.WorkHistory AR ( NOLOCK )
			WHERE AR.AboutTypeID = @AboutTypeID
				AND AR.ReferenceID = C.CANDIDATE_ID
		) WorkHistoryCount,
		(
			SELECT COUNT(*)
			FROM dbo.EducationHistory EH ( NOLOCK )
			WHERE EH.AboutTypeID = @AboutTypeID
				AND EH.ReferenceID = C.CANDIDATE_ID
		) EducationHistoryCount,
		(
			SELECT	count(distinct il.InvoiceID)
			FROM	dbo.InvoiceLine il
			INNER JOIN dbo.TimesheetLineAttribution tla ON il.InvoiceLineID = tla.InvoiceLineID
			INNER JOIN dbo.Timesheet t ON tla.TimesheetID = t.TimesheetID
			INNER JOIN dbo.Match m ON t.MatchID = m.MATCH_ID
			WHERE m.CANDIDATE_ID = C.CANDIDATE_ID
		) InvoiceCount,
		CONVERT(varchar(max),
			(
				SELECT
					Field.FieldID [@ID],
					Field.TypeID AS [@TypeID],
					Field.Name AS [@Name],
					Field.UniqueName AS [@UniqueName],
					Field.IsRequired AS [@IsRequired] ,
					Field.PlaceHolderID AS [@PlaceHolderID],
					Field.SortOrder AS [@SortOrder],
					COALESCE(Field.VisibleTo, SPACE(0)) AS [@VisibleTo],
					COALESCE(Field.DefaultValue, SPACE(0)) AS [@DefaultValue],
								COALESCE(cfv.ValueID, SPACE(0)) AS [@ValueID],
					COALESCE(cfv.Value, SPACE(0)) AS [Value],
					(
						SELECT 
						enumValues.EnumValue [@EnumValue],
						enumValues.EnumText [@EnumText],
						enumValues.RecordID [@RecordID],
						ISNULL(enumValues.SortOrder, 1) [@EnumSortOrder]
						FROM dbo.CustomFieldEnumValues enumValues 
						WHERE enumValues.FieldID = Field.FieldID
						ORDER BY enumValues.EnumText
						FOR XML PATH('ListItem'), TYPE  --TYPE so that it is nested as xml and not as text
					) [ListItems]
				FROM dbo.CustomFields AS Field
					LEFT OUTER JOIN dbo.CustomFieldValues cfv
						ON Field.FieldID = cfv.FieldID
						AND cfv.ReferenceID = @localReferenceID
				WHERE Field.AboutTypeID = @AboutTypeID
				ORDER BY Field.SortOrder ASC, Field.Name
				FOR XML PATH('Field'), ROOT('Fields')
			)
		) AS CustomFieldValues ,
		CONVERT(varchar(max),
			(
				SELECT
					AdditionalOwners.OwnerID [@OwnerID],
					OwnerRecruiters.FIRST_NAME AS [@OwnerFirstName],
					OwnerRecruiters.LAST_NAME AS [@OwnerLastName],
					isnull(AdditionalOwners.OwnerTypeID, '') AS [@OwnerTypeID],
					isnull(OwnerTypes.Name, '') AS [@OwnerTypeName]
				FROM dbo.AdditionalOwners AdditionalOwners
					INNER JOIN dbo.HR_REPRESENTATIVE OwnerRecruiters
						ON AdditionalOwners.OwnerID = OwnerRecruiters.UserID
					LEFT OUTER JOIN dbo.AdditionalOwnerTypes OwnerTypes
						ON AdditionalOwners.OwnerTypeID = OwnerTypes.OwnerTypeID
				WHERE AdditionalOwners.AboutTypeID = @AboutTypeID
					AND AdditionalOwners.ReferenceID = @localReferenceID
				ORDER BY isnull(AdditionalOwners.OwnerTypeID, 0)
				FOR XML PATH('AdditionalOwner'), ROOT('AdditionalOwners')
			)
		) AS AdditionalOwners,
		CONVERT(varchar(max),
			(
				SELECT
					RA.RelocationID [@RelocationID],
					RA.Name AS [@RelocationName]
				FROM dbo.CandidateRelocationAreas CRA
					INNER JOIN dbo.RelocationAreas RA
						ON CRA.RelocationID = RA.RelocationID
				WHERE CRA.CandidateID = @localReferenceID
				ORDER BY RA.Name
				FOR XML PATH('RelocationArea'), ROOT('RelocationAreas')
			)
		) AS RelocationAreas,
		CONVERT( varchar(max),
			(
				SELECT
					CandidateID [@CandidateID],
					CandidateFirstName AS [@CandidateFirstName],
					CandidateLastName AS [@CandidateLastName],
					CandidateReferenceID AS [@CandidateReferenceID],
					IsChecked AS [@IsChecked]
				FROM	dbo.fnGetReferredCandidates(@AboutTypeID, @localReferenceID)
				FOR XML PATH('Candidate'), ROOT('ReferredCandidates')
			)
		) AS ReferredCandidates,
		CONVERT(varchar(max),
			(
				SELECT
						DepartmentDimensionValues.ValueID [@DimensionValueID],
						DepartmentDimensionValues.ValueName AS [@DimensionValueName],
						isnull(ObjectStatusByDimension.StatusID, '') AS [@StatusID],
						isnull(CandidateStatus.STATUS_DESC, '') AS [@StatusName],
						isnull(CandidateStatus.Color, '') AS [@StatusColor],
						isnull(CandidateStatus.IconID, '') AS [@StatusIconID]
				FROM	dbo.DepartmentDimensionValues ( NOLOCK )
					LEFT OUTER JOIN dbo.ObjectStatusByDimension ( NOLOCK )
						ON ObjectStatusByDimension.DimensionValueID = DepartmentDimensionValues.ValueID AND ObjectStatusByDimension.AboutTypeID = @AboutTypeID AND ObjectStatusByDimension.ReferenceID = @localReferenceID
					LEFT OUTER JOIN dbo.CANDIDATE_STATUS CandidateStatus ( NOLOCK )
						ON ObjectStatusByDimension.StatusID = CandidateStatus.STATUS_ID
				WHERE	DepartmentDimensionValues.LevelID = @DimensionValueLevel
				ORDER BY DepartmentDimensionValues.ValueID
				FOR XML PATH('DimensionStatus'), ROOT('DimensionStatuses')
			)
		) AS DimensionStatuses,
		AdministrativeContact.HR_REP_ID AS AdministrativeContactID,
		C.AdministrativeContactUserID,
		AdministrativeContact.FIRST_NAME AdministrativeContactFirstName,
		AdministrativeContact.LAST_NAME AdministrativeContactLastName,
		C.UniqueID
	FROM
		dbo.CANDIDATE C ( NOLOCK )
		LEFT JOIN dbo.aspnet_Membership AM ( NOLOCK )
			ON C.UserID = AM.UserID
		LEFT OUTER JOIN dbo.CANDIDATE_STATUS candidateStatus ( NOLOCK )
			ON C.STATUS_ID = CandidateStatus.STATUS_ID
		LEFT OUTER JOIN dbo.CANDIDATE_RATING candidateRating ( NOLOCK )
			ON C.RATING = candidateRating.Rate_id
		LEFT OUTER JOIN dbo.HR_REPRESENTATIVE hrOwner ( NOLOCK )
			ON C.HR_REP_ID = hrOwner.HR_REP_ID
		LEFT OUTER JOIN dbo.HR_REPRESENTATIVE hrLead ( NOLOCK )
			ON C.HR_REP_ID_LEAD = hrLead.HR_REP_ID
		LEFT OUTER JOIN dbo.EEOCategories eeo ( NOLOCK )
			ON C.EEOCategoryID = eeo.CategoryID
		LEFT OUTER JOIN dbo.Ethnicities eth ( NOLOCK )
			ON C.EthnicityID = eth.CategoryID
		LEFT OUTER JOIN dbo.VeteranStatus vetStatus ( NOLOCK )
			ON C.VeteranStatusID = vetStatus.StatusID
		LEFT OUTER JOIN dbo.DisabilityStatuses disabilityStatuses ( NOLOCK )
			ON C.DisabilityStatusID = disabilityStatuses.ID
		LEFT OUTER JOIN dbo.CellphoneProvider cell ( NOLOCK )
			ON C.CellphoneProviderID = cell.ProviderID
		LEFT OUTER JOIN dbo.CNTR_BURDEN_TAX tax ( NOLOCK )
			ON C.TAX_ID = tax.TAX_ID
		LEFT OUTER JOIN dbo.EducationLevels educationLevels ( NOLOCK )
			ON C.EducationLevelID = educationLevels.EducationLevelID
		LEFT OUTER JOIN dbo.ClearanceLevels clearanceLevels ( NOLOCK )
			ON C.ClearanceLevelID = clearanceLevels.ClearanceLevelID
		LEFT OUTER JOIN dbo.COMPANY Tax1099Company ( NOLOCK )
			ON C.Tax1099CompanyID = Tax1099Company.COMPANY_ID
		LEFT OUTER JOIN dbo.Seeds seed ( NOLOCK )
			ON C.CANDIDATE_ID = seed.HarvestReferenceID  AND seed.HarvestAboutType = 6
		LEFT OUTER JOIN dbo.ThirdPartyMarketo marketo ( NOLOCK )
			ON C.CANDIDATE_ID = marketo.ReferenceID AND marketo.AboutTypeID = @AboutTypeID  
		LEFT OUTER JOIN dbo.MatchEndReasons terminationreason ( NOLOCK )
			ON C.TerminationReasonID = terminationreason.MatchEndReasonID 
		LEFT OUTER JOIN dbo.CANDIDATE MergedRecord ( NOLOCK )
			ON C.IsMergedTo = MergedRecord.CANDIDATE_ID
		LEFT JOIN dbo.Addresses DefaultAddress ( NOLOCK )
			ON C.DefaultAddressID = DefaultAddress.AddressID
		LEFT JOIN dbo.States DefaultAddressState ( NOLOCK )
			ON DefaultAddress.StateID = DefaultAddressState.StateID
		LEFT JOIN dbo.Countries DefaultAddressCountry ( NOLOCK )
			ON DefaultAddress.CountryID = DefaultAddressCountry.CountryID
		LEFT JOIN dbo.Regions DefaultAddressRegion ( NOLOCK )
			ON DefaultAddress.RegionID = DefaultAddressRegion.RegionID
		LEFT JOIN dbo.Entities OwnerEntity ( NOLOCK )
			ON C.OwnerEntityID = OwnerEntity.EntityID
		LEFT JOIN dbo.HR_REPRESENTATIVE AdministrativeContact (NOLOCK) 
			ON C.AdministrativeContactUserID = AdministrativeContact.UserID
		JOIN (
			SELECT
				C.CANDIDATE_ID ,
				COUNT(T.TimesheetID) CountTimesheet ,
				ISNULL(SUM(T.TotalHours), 0) TotalHoursWorked ,
				ISNULL(( SUM(T.Billed) - SUM(T.Paid)
					- SUM(T.TaxEstimated)
					- SUM(T.WorkComp) - SUM(T.Admin) ), 0) TotalProfitGenerated
			FROM dbo.CANDIDATE C ( NOLOCK )
				LEFT JOIN dbo.Match M ( NOLOCK )
					ON C.CANDIDATE_ID = M.CANDIDATE_ID
				LEFT JOIN dbo.POSITION P ( NOLOCK )
					ON M.POSITION_ID = P.POSITION_ID
				LEFT JOIN dbo.Timesheet T ( NOLOCK )
					ON M.MATCH_ID = T.MatchID
					AND EXISTS (
						SELECT TimesheetLineEntryID
						FROM dbo.TimesheetLineEntry
						WHERE
							TimesheetID = T.TimesheetID
							AND StatusID >= 3
					)
			GROUP BY C.CANDIDATE_ID
		) TC
			ON C.CANDIDATE_ID = TC.CANDIDATE_ID
		OUTER APPLY (
			SELECT TOP 1
				NI.NoteID LastNoteID ,
				NI.Body LastNoteBody ,
				NI.CreatedBy LastNoteCreatedBy ,
				NI.CreatedOn LastNoteCreatedOn ,
				COALESCE(HRI.FIRST_NAME, '') + ' ' + COALESCE(HRI.LAST_NAME, '') LastNoteCreatedByName
			FROM dbo.NewNotes NI ( NOLOCK )
				JOIN dbo.HR_REPRESENTATIVE HRI ( NOLOCK ) ON NI.CreatedBy = HRI.UserID
				JOIN dbo.NoteActions NA ( NOLOCK ) ON NA.ActionID = NI.ActionID
			WHERE NI.NoteID = C.LastNoteID
				AND NA.Permission IS NULL
		) LastNote
		OUTER APPLY (
			SELECT TOP 1
				RI.NoteID LastNoteIDByUser ,
				NI.Body LastNoteBodyByUser ,
				NI.CreatedBy ,
				NI.CreatedOn LastNoteCreatedOnByUser ,
				RI.RecordID
			FROM
				dbo.NoteReferences RI ( NOLOCK )
				JOIN dbo.NewNotes NI ( NOLOCK ) ON RI.NoteID = NI.NoteID
				JOIN dbo.NoteActions NA ( NOLOCK ) ON NA.ActionID = NI.ActionID
			WHERE
				RI.AboutTypeID = @AboutTypeID
				AND RI.ReferenceID = C.CANDIDATE_ID
				AND NI.CreatedBy = @localUserID
			ORDER BY
				NI.CreatedOn DESC ,
				RI.NoteID DESC
		) LastNoteByUser
	WHERE C.CANDIDATE_ID = @localReferenceID

END

GO

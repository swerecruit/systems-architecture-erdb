﻿IF EXISTS(SELECT * FROM	dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[fnConcatenateCommunicationsByCategory]'))
EXECUTE('DROP FUNCTION [dbo].[fnConcatenateCommunicationsByCategory]')

PRINT 'Creating/Updating FUNCTION [dbo].[fnConcatenateCommunicationsByCategory]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fnConcatenateCommunicationsByCategory] (@ReferenceID int, @AboutTypeID int, @CategoryID int, @IncludePrimary bit)
RETURNS TABLE
AS
RETURN
SELECT STUFF((
	SELECT ';' + ISNULL(comm.value,'')
	FROM	dbo.Communications comm 
	INNER JOIN dbo.CommunicationTypes ct on comm.CommunicationTypeID=ct.CommunicationTypeID
	WHERE ct.CommunicationCategoryID=@CategoryID 
	AND comm.ReferenceID = @ReferenceID 
	AND comm.AboutTypeID = @AboutTypeID 
	AND (comm.IsPrimary <> 1 or @IncludePrimary = 1)
	FOR XML PATH('')
)
,1,1,'') AS Communication

GO


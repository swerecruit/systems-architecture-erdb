﻿IF not exists( select * from sys.columns where name = 'VisaID' and object_id = object_id('CANDIDATE') ) 
BEGIN
	ALTER TABLE [dbo].[CANDIDATE] ADD VisaID nvarchar(50) NULL
END

IF not exists( select * from sys.columns where name = 'VisaExpirationDate' and object_id = object_id('CANDIDATE') ) 
BEGIN
	ALTER TABLE [dbo].[CANDIDATE] ADD VisaExpirationDate date NULL
END

﻿if not exists( select * from sys.columns where name = 'LayoutJson' and object_id = object_id('dbo.SavedSearches') )
	alter table dbo.SavedSearches add LayoutJson nvarchar(max) null
GO

﻿IF not exists( select * from sys.columns where name = 'RequiredVisa' and object_id = object_id('CITIZENSHIP') ) 
BEGIN
	ALTER TABLE [dbo].[CITIZENSHIP] ADD RequiredVisa bit NOT NULL CONSTRAINT DF_CITIZENSHIP_RequiredVisa DEFAULT ((0))
END

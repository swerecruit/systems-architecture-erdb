﻿exec dbo.spAddNonclusteredIndex 
	@Schema='dbo',
	@Table='WipDistribution', 
	@IndexColumns='TimesheetLineAttributionID', 
	@IncludedColumns='DistributionTypeID,AccountTypeID,AccountNumber,DebitAmount,CreditAmount,CreatedOn', 
	@IndexName='IX_WipDistribution_TimesheetLineAttributionID'

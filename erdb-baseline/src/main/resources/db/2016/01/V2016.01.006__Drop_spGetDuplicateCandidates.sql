﻿-- deprecated see: DEV-8137
IF OBJECT_ID('dbo.spGetDuplicateCandidates') IS NOT NULL
BEGIN
	DROP PROCEDURE dbo.spGetDuplicateCandidates;
END;

This module contains scripts for migrating an erecruit database.

Prerequisites
=============

You will need to have a working JRE/JDK on your machine at version
1.7 or higher. Make sure that JAVA_HOME/PATH environment variables
set up appropriately.

Running the Migration Tool
==========================

Full documentation for Flyway is available at http://flywaydb.org/documentation/commandline/


EXEC tSQLt.NewTestClass 'testSqlValidation';
GO

IF OBJECT_ID('testSqlValidation.[test Sql dependencies by running sp_refreshsqlmodule on all sys.sql_modules in dbo schema', 'P') IS NOT NULL
	DROP PROCEDURE testSqlValidation.[test Sql dependencies by running sp_refreshsqlmodule on all sys.sql_modules in dbo schema];
GO
 
CREATE PROCEDURE testSqlValidation.[test Sql dependencies by running sp_refreshsqlmodule on all sys.sql_modules in dbo schema]
AS
BEGIN
	SET NOCOUNT ON

	-- table variable to store procedure names
	DECLARE @tblObjects TABLE (ObjectID INT IDENTITY(1,1), ObjectName sysname, ObjectType char(2))

	-- get the list of stored procedures, functions and views
	INSERT INTO @tblObjects(ObjectName,ObjectType)
		SELECT '[' + sc.[name] + '].[' + obj.name + ']',obj.[type]
		FROM sys.objects obj
		INNER JOIN sys.sql_modules m ON m.object_id = obj.object_id
		INNER JOIN sys.schemas sc ON sc.schema_id = obj.schema_id
		--WHERE obj.[type] IN ('P', 'FN', 'V') -- procedures, functions, views
		WHERE m.is_schema_bound = 0
		AND obj.schema_id = 1

	-- counter variables
	DECLARE @Count INT, @Total INT
	SELECT @Count = 1	
	SELECT @Total = COUNT(*) FROM @tblObjects

	DECLARE @ErrorCount SMALLINT = 0;
	DECLARE @MaxErrorCount SMALLINT = 10; -- in case you want to limit
	DECLARE @ErrorMessage NVARCHAR(max) = '';

	DECLARE @ObjectName sysname
	DECLARE @ObjectType char(2)

	-- start the loop
	WHILE (@Count <= @Total) AND (@ErrorCount < @MaxErrorCount)
	BEGIN

		SELECT @ObjectName = ObjectName, @ObjectType = ObjectType
		FROM @tblObjects
		WHERE ObjectID = @Count

		--PRINT 'Refreshing... ' + @ObjectName

		BEGIN TRY
			EXEC sp_refreshsqlmodule @ObjectName
		END TRY
		BEGIN CATCH
			--PRINT 'EXEC sp_refreshsqlmodule ''' + @ObjectName + ''''
			--PRINT 'Validation failed for : ' + @ObjectName + ' (' + @ObjectType + ')' + ', Error:' + ERROR_MESSAGE() + CHAR(13)
			SET @ErrorMessage += 'Validation failed for : ' + @ObjectName + ' (' + @ObjectType + ')' + ', Error:' + ERROR_MESSAGE() + ';';			
			SET @ErrorCount += 1;								
		END CATCH
   
		SET @Count = @Count + 1
	END;

	IF @ErrorCount > 0
		EXEC tSQLt.Fail @ErrorMessage;
	END;
GO
 
--EXEC tSQLt.Run 'testSqlValidation.testSqlValidation';
--GO

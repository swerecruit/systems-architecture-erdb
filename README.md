# Quick Start

Since you're reading this, we assume you've been able to check out this project from Git. Next steps are:

0. Ensure JDK and Apache Maven are installed on your machine, and are
   set up properly. You'll need:

   * JDK 7 or higher;
   * Maven 3.x or higher

1. Create a database in SSMS that you'd like to use with this tool.

2. Create a file named `build.properties` at the top level of this
   project. Use the file `build.properties.template` as an example and
   change the URL/username/password values to match the DB you created
   in step #2

3. `mvn compile` to migrate the database identified in build.properties.

4. `mvn package` to build a Flyway distribution

# Module structure

* erdb-baseline -- Flyway distribution for the core schema

# Gotchas

- When running flyway, the JVM needs to have -Djava.library.path=<path
  to directory containing the right sqljdbc_auth.dll for your
  architecture.


